"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MatrizJuego = void 0;
class MatrizJuego {
    constructor() {
        this.Matriz = [-1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1, -1, 0];
    }
    setPunteo(Pos, Valor) {
        this.Matriz[Number(Pos)] = Valor;
    }
    getPunteo(Pos) {
        return this.Matriz[Number(Pos)];
    }
    llena() {
        for (var i = 0; i < this.Matriz.length; i++) {
            if (this.Matriz[i] == -1) {
                return false;
            }
        }
        return true;
    }
    punteoFinal() {
        var punteo = 0;
        for (var i = 0; i < this.Matriz.length; i++) {
            if (i == 6) {
                //Guardamos la suma de la
                //primera parte del juego
                this.Matriz[i] = punteo;
            }
            else if (i == 7) {
                //Verificamos si tiene bonus
                if (this.Matriz[6] >= 63) {
                    punteo = punteo + 35;
                    this.Matriz[i] = 35;
                }
            }
            else if (i == 15) {
                //Tenemos el total
                break;
            }
            else {
                punteo = punteo + Number(this.Matriz[i]);
            }
        }
        return punteo;
    }
    toString() {
        return this.Matriz.toString();
    }
}
exports.MatrizJuego = MatrizJuego;
/**
 * Matriz[
 *  00:UNOS,
 *  01:DOS,
 *  02:TRES,
 *  03:CUATROS,
 *  04:CINCOS,
 *  05:SEISES,
 *  06:SUMA,
 *  07:BONUS,
 *  08:3IGUAL,
 *  09:4IGUAL,
 *  10:CASALLENA,
 *  11:PEQUENIA,
 *  12:GRANDE,
 *  13:CHANCE,
 *  14:YAHTZEE,
 *  15:TOTAL
 * ]
 */ 
