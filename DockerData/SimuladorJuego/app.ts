var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var fs = require('fs');

import { Response, Request } from "express";
import { Juego } from "./LogicaIA/Juego";

var app = express();
app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));


/*Variables de entorno donde correra la API*/
const ip = process.env.IP || "182.18.7.8";
const port = process.env.PORT || 7000;


app.post('/simular', (req:Request, res:Response) => {
    var id = req.body.id.toString();
    var jug = req.body.jugadores; //Es un array de tamaño 2
    var jugador1 = new Juego(jug[0].toString());
    var jugador2 = new Juego(jug[1].toString());

    fs.appendFile('./Log/bitacora.txt',id + ":\n", function(){});
    console.log(id + ":\n");

    while(!(jugador1.juegoTerminado() && jugador2.juegoTerminado())){
        var log1 = jugador1.tomarTurno();
        var log2 = jugador2.tomarTurno();

        fs.appendFile('./Log/bitacora.txt',"\t" + log1 + "\n\t" + log2 + "\n", function(){});
        console.log("\t" + log1 + "\n\t" + log2 + "\n");
    }

    var resultado = [jugador1.puntajeFinal(), jugador2.puntajeFinal()];

    res.send(JSON.parse(JSON.stringify( {resultado: resultado} )));
});


app.listen(port, ip, async () => {
    console.log('Se escucha en el puerto: %d y con la ip: %s', port, ip);
});