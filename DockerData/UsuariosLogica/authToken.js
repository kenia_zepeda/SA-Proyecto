const jwt = require("jsonwebtoken");
const fs = require('fs');
const axios = require('axios');

var publicKEY = fs.readFileSync('./public.key', 'utf8');

var verifyOptions = {
  algorithm: "RS256"
};

const scopes_validos = [
  'usuarios.login',
  'usuarios.jugadores.get',
  'usuarios.jugadores.post',
  'usuarios.jugadores.put'
];

const jwtip = process.env.JWTIP || "182.18.7.14";
const jwtport = process.env.JWTPORT || 10000;

/*Variables de JWT para validar los scopes*/
const jwtid = process.env.IDJWT || "b04cb390-1012-11eb-adc1-0242ac120002";
const secret = process.env.SECRET || "e953be0e56e43b2db4485fc880991f";

async function authtoken(req, res, next){ 
  var url = 'http://'+jwtip+':'+jwtport+'/token';

  var token = await axios.post(url+"?id="+jwtid+"&secret="+secret)
  .catch(function (error){
    console.log(error);
  });

  if(token){
    var legit = jwt.verify(token.data.jwt, publicKEY, verifyOptions);

    if(legit){
      var alcanze_servicio = legit.scope;

      if(alcanze_servicio.length == scopes_validos.length &&
        !alcanze_servicio.some((v) => scopes_validos.indexOf(v) < 0)){

          console.log("Acceso concedido");
          next();
      }else{
          console.log(false);
          res.status(406).send("Acceso denegado");
      }
    }else{
      res.status(406).send("Error de token");
    }
  }else{
    console.log("Token no valido");
  }
}

module.exports.authtoken = authtoken;