const fs   = require('fs');
const express = require("express");
var cookieParser = require('cookie-parser');
const bodyparser = require('body-parser');
var jugadorRouter = require('./routes/jugador');
var loginRouter = require('./routes/login');

const app = express();
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());


const ip = process.env.IP || "182.18.7.13";
const port = process.env.PORT || 9000;

app.use('/login', loginRouter);
app.use('/jugadores', jugadorRouter);

app.get("/", (req, res) => {
    res.send("SERVIDOR USUARIOS ENCENDIDO");
});

app.listen(port, ip, async () => {
    console.log('Se escucha en el puerto: %d y con la ip: %s', port, ip);
});