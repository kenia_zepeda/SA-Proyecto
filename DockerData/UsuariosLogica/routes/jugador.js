var express = require("express");

var connection = require('../connection');
var auth = require('../authToken');

var router = express.Router();


router.get("/:id", auth.authtoken, (req, res) => {
    var user_id = req.params['id'];

    if (user_id == -1){
        var sql = `SELECT id,nombres FROM usuario`;

        connection.query(sql, function (err, result) {
            if (err) throw err;
            res.status(200).json(result)
        });

    }else{
        var sql = `SELECT * FROM usuario WHERE id='${user_id}'`;

        connection.query(sql, function (err, result) {
            if (err) throw err;

            if (result.length == 0){
                res.sendStatus(404);
            }else{
                if(result[0].administrador == 0){
                    result[0].administrador = false;
                }else{
                    result[0].administrador = true;
                }
                res.status(200).json(result[0]);
            }
        });
    }
});



router.put("/:id",auth.authtoken, (req, res) => {
    var user_id = req.params['id'];

    var sql = `SELECT * FROM usuario WHERE id='${user_id}'`;

    connection.query(sql, function (err, result){
        if (err) res.sendStatus(406);

        if (result.length == 0){
            res.sendStatus(404);
        }else{
            var admin = 0;

            if (req.body.administrador == true){
                admin = 1;
            }

            var sql = `UPDATE usuario SET nombres='${req.body.nombres}',apellidos='${req.body.apellidos}',email='${req.body.email}',password='${req.body.password}',administrador='${admin}' WHERE id='${user_id}'`;
            
            connection.query(sql, function (err, result) {
                if (err) res.sendStatus(406);

                if (result.length == 0){
                    res.sendStatus(404);
                }else{
                    if(admin == 1){
                        admin = true;
                    }else{
                        admin = false;
                    }

                    res.status(201).json({
                        id: user_id,
                        nombres: req.body.nombres,
                        apellidos: req.body.apellidos,
                        email: req.body.email,
                        password: req.body.password,
                        administrador: admin
                    });
                }
            });
        }   
    });
});



router.post("/",auth.authtoken, (req, res) => {
    var admin = 0;

    if(req.body.administrador == true){
        admin = 1;
    }

    var sql = `INSERT INTO usuario (nombres,apellidos,email,password,administrador) VALUES ('${req.body.nombres}','${req.body.apellidos}','${req.body.email}','${req.body.password}','${admin}')`;
    
    connection.query(sql, function (err, result) {
        if (err) res.sendStatus(406);

        if(admin == 1){
            admin = true;
        }else{
            admin = false;
        }

        res.status(201).json({
            id: result.insertId,
            nombres: req.body.nombres,
            apellidos: req.body.apellidos,
            email: req.body.email,
            password: req.body.password,
            administrador: admin
        });
    });
});

module.exports = router;