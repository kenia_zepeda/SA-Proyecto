//require("dotenv").config();
//'use strict';
var mongoose = require('mongoose');
const fs = require('fs');
const express = require("express");
const jwt = require("jsonwebtoken");
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.json());

var privateKEY = fs.readFileSync('./private.key', 'utf8');
var publicKEY  = fs.readFileSync('./public.key', 'utf8');

const microservicios = require('./schemas/services');
const scopes = require('./schemas/scopes');

/*Variables de entorno para la API AuthServer*/
const ip = process.env.IP || "182.18.7.14";
const port = process.env.PORT || 10000;
const dbip = process.env.JWTDBIP || "182.18.7.4";
const dbport = process.env.JWTDBPORT || 11000;
const mongoURL = "mongodb://mongodb:27017/jwt";

var exp = Math.floor(new Date().getTime() / 1000) + (60 * 60 * 24 * 2);

let payload = {
    alg: 'RS256',
    typ: "JWT",
    exp: exp,
    scope: []
}

var signOptions = {
  algorithm:  "RS256"
};

var verifyOptions = {
  algorithm:  "RS256"
};

//====================================== MONGODB ======================================//
mongoose.connect(mongoURL,{
  useNewUrlParser: true,
  useUnifiedTopology: true
}).catch(error => console.log("Error:",error));


//======================================== AUTH ========================================//

app.post("/token", (req, res) => {
  const id_servicio = req.query.id;
  const secret = req.query.secret;

  if(id_servicio == null || secret==null) return res.sendStatus(400);
  
  microservicios.findOne({id:id_servicio},{_id:0,id:1,secret:1}, function(err, result){
    if(err){
      res.sendStatus(400);
    }else{
      if(result != null){
        if(result.id == id_servicio && result.secret == secret){
          scopes.findOne({id:id_servicio},{_id:0,scope:1}, function(err, result){
            if(err){
              res.sendStatus(400)
            }else{
              var js = JSON.stringify(result);
              var obj = JSON.parse(js);        
              payload.scope = obj.scope;          
              let accessToken = jwt.sign(payload, privateKEY, signOptions);
              res.status(201).json({jwt:accessToken});            
            }
          });
        }else{
          res.sendStatus(400);
        }
      }else{
        res.sendStatus(400);
      }
    }
  });
});


app.post('/decode', (req, res)=>{
  var legit = jwt.verify(req.body.token, publicKEY, verifyOptions);
  res.json(legit)
});


app.get("/", (req, res) => {
  res.send("SERVIDOR JWT ENCENDIDO");
});


app.listen(port, ip, async () => {
  console.log('Se escucha en el puerto: %d y con la ip: %s', port, ip);
});