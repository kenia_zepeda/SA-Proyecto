"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var fs = require('fs');
var app = express();
app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
/*Variables de entorno donde correra la API*/
const ip = process.env.IP || "182.18.7.7";
const port = process.env.PORT || 5000;
app.get('/tirar/:cantidad', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var array = [];
    var cantidad = Number(req.params.cantidad);
    if (cantidad < 0) {
        res.status(400).send("Numero de dados a tirar no valido");
    }
    for (var i = 0; i < cantidad; i++) {
        array.push(Math.floor(Math.random() * (7 - 1) + 1));
    }
    fs.appendFile('./Log/bitacora.txt', array.toString() + "\n", function () { });
    console.log(array.toString());
    res.status(200).send(JSON.parse(JSON.stringify({ dados: array })));
}));
app.listen(port, ip, () => __awaiter(void 0, void 0, void 0, function* () {
    console.log('Dados se escucha en el puerto: %d y con la ip: %s', port, ip);
}));
