
function inittablero(){
    var logeado = sessionStorage.getItem('logueado');
    if(!logeado){
        window.location.href = "../";
    }

    //inicializamos la partida
    document.getElementById("btnlanzar").disabled = false;
    document.getElementById("btnelegir").disabled = false;
    document.getElementById('btnactualizar').style.visibility = "hidden";
    sessionStorage.setItem('partida-terminada',false);
    limpiar();

    //agregamos el nombre de usuario
    var nombre = sessionStorage.getItem('nombre-usuario');
    document.getElementById("nombre_user").innerText = nombre;
}

function regresar(){
    var terminada = sessionStorage.getItem('partida-terminada');

    if(!terminada){
        alert("Aun no terminas la partida");
        return;
    }

    //limpiar mi jugada y guardar score
    guardarPartida();
    window.location.href = "../index";
}

//======================================== LANZAR DADOS ========================================//

function lanzar(){
    //obtenemos el numero de tiros que nos quedan
    var numtiros = sessionStorage.getItem('num-tiros');

    //verificamos si aun tenemos tiros
    if(numtiros <= 0){
        alert("Ya no tienes tiros");
        return;
    }

    //si aun nos quedan tiros enviamos la peticion
    fetch('../tirar', {
        method: 'POST',
        body: JSON.stringify({"Cant":5}),
        headers:{
          'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => setDados(response));

    //restamos la cantidad de tiros permitidos
    sessionStorage.setItem('num-tiros', numtiros - 1);
}


function setDados(response){
    //de la respuesta obtenemos el array de dados
    var dados = JSON.parse(JSON.stringify(response)).dados;

    //actualizamos los dados
    for(var i = 0; i < dados.length; i++){
        //se actualizan unicamente los permitidos por el usuario
        if(!document.getElementById("rd"+(i + 1)).checked){
            var dado = dados[i].toString();
            document.getElementById("dado" + (i + 1)).src = "../style/img/dado"+dado+".jpg";
        }
    }
}


//=================================== PEDIMOS POSIBILIDADES ===================================//

function elegir(){
    //elegimos los dados actuales
    var dados = [];
    dados.push(document.getElementById("dado1").src);
    dados.push(document.getElementById("dado2").src);
    dados.push(document.getElementById("dado3").src);
    dados.push(document.getElementById("dado4").src);
    dados.push(document.getElementById("dado5").src);

    for(var i=0; i< dados.length; i++){
        dados[i] = dados[i].replace(".jpg","");
        var arr = Array.from(dados[i]);
        dados[i] = arr[arr.length -1];
    }

    //vemos que usuario es
    var id  = sessionStorage.getItem('id-partida');
    var jid = sessionStorage.getItem('id-usuario');

    //enviamos la peticion
    fetch('../tomarTiro', {
        method: 'POST',
        body: JSON.stringify({"Pid":id,"Jid":jid,"Dados":dados}),
        headers:{
          'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => setjugada(response));


    //actualizamos la jugada del contrincante
    pedirTablero();
}

function setjugada(response){
    //vamos a mostrar las posibilidades
    var resp = JSON.parse(JSON.stringify(response));
    var array = resp.resultado;
    var turno = resp.turno;
    var comp = resp.completos;

    if(!turno){
        limpiar();
        alert("Lo sentimos aun no es tu turno");
        return;
    }

    if(!comp){
        limpiar();
        alert("Lo sentimos aun no llega el contrincante");
        return;
    }

    //si es nuestro turno y estan ambos jugadores
    var cont = 1;
    var hay_jugada = false;
    array.forEach(element => {
        if(element > 0){
            hay_jugada = true;
            var btn = document.createElement("button");
            btn.setAttribute('id','btn' + cont);
            btn.setAttribute('onclick','tomarpuntos(this);');
            btn.setAttribute("class","d-inline-block btn btn-primary");
            btn.innerText = element;

            var jugada = document.getElementById("j" + cont);
            jugada.innerHTML = "";
            jugada.appendChild(btn);
        }
        cont ++;
    });

    //si no existe ninguna jugada saltamos turno
    if(!hay_jugada){
        var btn = document.createElement("button");
        btn.setAttribute('id','btn' + 0);
        btn.setAttribute('onclick','tomarpuntos(this);');
        btn.setAttribute("class","d-inline-block btn btn-primary");
        btn.innerText = -1;
        tomarpuntos(btn);
        limpiar();
        alert("Mala jugada saltas turno");
    }
}


//====================================== ELEGIMOS PUNTOS ======================================//

function tomarpuntos(btn){
    //vemos cual posicion del tablero es y los puntos
    var idb = btn.id.toString();
    var value = btn.innerText;
    var pos = (parseInt(idb.replace("btn","")) - 1).toString();

    //vemos que usuario es
    var id  = sessionStorage.getItem('id-partida');
    var jid = sessionStorage.getItem('id-usuario');

    //enviamos la peticion
    fetch('../elegirJugada', {
        method: 'POST',
        body: JSON.stringify({"Pid":id,"Jid":jid,"Pos":pos,"Pts":value}),
        headers:{
          'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => limJugada(response, idb, value));
}

function limJugada(response, idb, value){
    //si todo salio bien agregamos los puntos
    var resp = JSON.parse(JSON.stringify(response));
    var turno = resp.turno;
    var comp = resp.completos;
    var termino = resp.termino;

    if(!turno){
        limpiar();
        alert("Lo sentimos aun no es tu turno");
        return;
    }

    if(!comp){
        limpiar();
        alert("Lo sentimos aun no llega el contrincante");
        return;
    }

    var puntos = document.getElementById(idb.replace("btn","p"));
    puntos.innerHTML = value;
    limpiar();

    if(termino){
        tableroTerminado();
        document.getElementById("turno").innerText = "¡Terminaste!";
        sessionStorage.setItem('partida-terminada',true);
    }
}


//===================================== VER PUNTAJE FINAL =====================================//

function tableroTerminado(){
    //vemos que usuario es
    var id  = sessionStorage.getItem('id-partida');
    var jid = sessionStorage.getItem('id-usuario');

    //enviamos peticion
    fetch('../tableroTerminado', {
        method: 'POST',
        body: JSON.stringify({"Pid":id,"Jid":jid}),
        headers:{
          'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => mostrarTerminado(response));
}

function mostrarTerminado(response){
    //obtenemos el tablero final
    var array = JSON.parse(JSON.stringify(response)).resultado;

    //llenamos el tablero con la jugada final
    var cont = 1;
    array.forEach(element => {
        var jugada = document.getElementById("p" + cont);
        jugada.innerHTML = "";
        jugada.innerHTML = element;
        cont ++;
    });

    //deshabilitamos los botones para dados y posibilidades
    document.getElementById("btnlanzar").disabled = true;
    document.getElementById("btnelegir").disabled = true;
    
    //habilitamos el boton para actualizar la jugada del contrincante
    document.getElementById('btnactualizar').style.visibility = "visible";
}

function guardarPartida(){
    //vemos que usuario es
    var id  = sessionStorage.getItem('id-partida');
    var jid = sessionStorage.getItem('id-usuario');

    //enviamos peticion
    fetch('../terminarJuego', {
        method: 'POST',
        body: JSON.stringify({"Pid":id,"Jid":jid}),
        headers:{
          'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response));
}


//==================================== TABLERO ADVERSARIO ====================================//

function pedirTablero(){
    //vemos que usuario es
    var id  = sessionStorage.getItem('id-partida');
    var jid = sessionStorage.getItem('id-usuario');

    //enviamos la peticion
    fetch('../tableroAdversario', {
        method: 'POST',
        body: JSON.stringify({"Pid":id,"Jid":jid}),
        headers:{
          'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => setjugadaAdv(response));
}

function setjugadaAdv(response){
    //obtenemos el tablero del adversario
    var array = JSON.parse(JSON.stringify(response)).adversario;

    //actualizamos el tablero del adversario
    var cont = 1;
    array.forEach(element => {
        if(element > 0){
            var jugada = document.getElementById("a" + cont);
            jugada.innerHTML = "";
            jugada.innerHTML = element;
        }
        cont ++;
    });
}


//========================================= LIMPIEZA =========================================//

function limpiar(){
    //limpiamos las posibles jugadas
    for(var i=1; i<17; i++){
        document.getElementById("j" + i).innerHTML = "";
    }

    //limpiamos los dados
    for(var i = 0; i < 5; i++){
        document.getElementById("dado" + (i + 1)).src = "../style/img/dado0.jpg";
    }

    //restablecemos los tiros permitidos
    sessionStorage.setItem('num-tiros', 3);

    //limpiamos la seleccion de dados
    document.getElementById("rd1").checked = false;
    document.getElementById("rd2").checked = false;
    document.getElementById("rd3").checked = false;
    document.getElementById("rd4").checked = false;
    document.getElementById("rd5").checked = false;
}