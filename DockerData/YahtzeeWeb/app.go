package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
)

//================================ URL DE MICROSERVICIOS ================================//
var juegoURL = ""
var dadosURL = ""
var usersURL = ""

//============================ SERVICIOS DE LA PARTE LOGICA ============================//

type inicio struct {
	Email string
	Pass  string
}

type pendiente struct {
	Jid string
}

type accion struct {
	Pid string
	Jid string
}

type tirard struct {
	Pid   string
	Jid   string
	Dados []string
}

type elegir struct {
	Pid string
	Jid string
	Pos string
	Pts string
}

type dados struct {
	Cant int
}

func loguearse(w http.ResponseWriter, r *http.Request) {
	//var url = usersURL + "/login/"
	var url = juegoURL + "/login/"

	var decoder = json.NewDecoder(r.Body)
	var i inicio
	err := decoder.Decode(&i)
	if err != nil {
		//panic(err)
	}

	var jsonStr = []byte(`{"email":"` + i.Email + `","password":"` + i.Pass + `"}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
	}

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	fmt.Println(string(bodyBytes))

	fmt.Fprintf(w, string(bodyBytes))
}

func partidaPendiente(w http.ResponseWriter, r *http.Request) {
	var url = juegoURL + "/partidaPendiente/"

	var decoder = json.NewDecoder(r.Body)
	var j pendiente
	err := decoder.Decode(&j)
	if err != nil {
		//panic(err)
	}

	var jsonStr = []byte(`{"jid":` + j.Jid + `}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
	}

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	fmt.Println(string(bodyBytes))

	fmt.Fprintf(w, string(bodyBytes))
}

func agregarJugador(w http.ResponseWriter, r *http.Request) {
	var url = juegoURL + "/agregarJugador/"

	var decoder = json.NewDecoder(r.Body)
	var j accion
	err := decoder.Decode(&j)
	if err != nil {
		//panic(err)
	}

	var jsonStr = []byte(`{"id":"` + j.Pid + `","jid":` + j.Jid + `}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
	}

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	fmt.Println(string(bodyBytes))

	fmt.Fprintf(w, string(bodyBytes))
}

func tomarTiro(w http.ResponseWriter, r *http.Request) {
	var url = juegoURL + "/tomarTiro/"

	var decoder = json.NewDecoder(r.Body)
	var t tirard
	err := decoder.Decode(&t)
	if err != nil {
		//panic(err)
	}

	var jsonStr = []byte(`{"id":"` + t.Pid + `","jid":` + t.Jid + `,"dados":[` + strings.Join(t.Dados, ",") + `]}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
	}

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	fmt.Println(string(bodyBytes))

	fmt.Fprintf(w, string(bodyBytes))
}

func elegirJugada(w http.ResponseWriter, r *http.Request) {
	var url = juegoURL + "/elegirJugada/"

	var decoder = json.NewDecoder(r.Body)
	var e elegir
	err := decoder.Decode(&e)
	if err != nil {
		//panic(err)
	}

	var jsonStr = []byte(`{"id":"` + e.Pid + `","jid":` + e.Jid + `,"pos":` + e.Pos + `,"pts":` + e.Pts + `}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
	}

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	fmt.Println(string(bodyBytes))

	fmt.Fprintf(w, string(bodyBytes))
}

func tableroAdversario(w http.ResponseWriter, r *http.Request) {
	var url = juegoURL + "/tableroAdversario/"

	var decoder = json.NewDecoder(r.Body)
	var j accion
	err := decoder.Decode(&j)
	if err != nil {
		//panic(err)
	}

	var jsonStr = []byte(`{"id":"` + j.Pid + `","jid":` + j.Jid + `}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
	}

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	fmt.Println(string(bodyBytes))

	fmt.Fprintf(w, string(bodyBytes))
}

func tableroTerminado(w http.ResponseWriter, r *http.Request) {
	var url = juegoURL + "/tableroTerminado/"

	var decoder = json.NewDecoder(r.Body)
	var j accion
	err := decoder.Decode(&j)
	if err != nil {
		//panic(err)
	}

	var jsonStr = []byte(`{"id":"` + j.Pid + `","jid":` + j.Jid + `}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
	}

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	fmt.Println(string(bodyBytes))

	fmt.Fprintf(w, string(bodyBytes))
}

func terminarJuego(w http.ResponseWriter, r *http.Request) {
	var url = juegoURL + "/terminarJuego/"

	var decoder = json.NewDecoder(r.Body)
	var j accion
	err := decoder.Decode(&j)
	if err != nil {
		//panic(err)
	}

	var jsonStr = []byte(`{"id":"` + j.Pid + `","jid":` + j.Jid + `}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
	}

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	fmt.Println(string(bodyBytes))

	fmt.Fprintf(w, string(bodyBytes))
}

func tirar(w http.ResponseWriter, r *http.Request) {
	var decoder = json.NewDecoder(r.Body)
	var d dados
	err := decoder.Decode(&d)
	if err != nil {
		//panic(err)
	}

	var url = dadosURL + "/tirar/" + strconv.Itoa(d.Cant)

	resp, err := http.Get(url)
	if err != nil {
		//panic(err)
	}

	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	fmt.Println(string(bodyBytes))

	fmt.Fprintf(w, string(bodyBytes))
}

//============================ SERVICIOS DE LA PARTE GRAFICA ============================//

func login(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseFiles("login.html"))
	t.Execute(w, "")
}

func tablero(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseFiles("tablero.html"))
	t.Execute(w, "")
}

func index(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseFiles("index.html"))
	t.Execute(w, "")
}

func main() {
	//================== URL JUEGO ==================//
	juegoip, defjip := os.LookupEnv("JUEGOIP")
	juegoport, defjport := os.LookupEnv("JUEGOPORT")

	if !defjip {
		juegoip = "182.18.7.9"
	}

	if !defjport {
		juegoport = "4000"
	}

	juegoURL = "http://" + juegoip + ":" + juegoport

	//================== URL DADOS ==================//
	dadosip, defdip := os.LookupEnv("DADOSIP")
	dadosport, defdport := os.LookupEnv("DADOSPORT")

	if !defdip {
		dadosip = "182.18.7.7"
	}

	if !defdport {
		dadosport = "5000"
	}

	dadosURL = "http://" + dadosip + ":" + dadosport

	//================== URL PROPIA ==================//
	ip, defip := os.LookupEnv("GOIP")
	port, defport := os.LookupEnv("GOPORT")

	if !defip {
		ip = "182.18.7.10"
	}

	if !defport {
		port = "3000"
	}

	//archivos estaticos
	http.Handle("/logic/", http.StripPrefix("/logic/", http.FileServer(http.Dir("logic/"))))
	http.Handle("/style/", http.StripPrefix("/style/", http.FileServer(http.Dir("style/"))))

	//endpoint para interfaz
	http.HandleFunc("/", login)
	http.HandleFunc("/index", index)
	http.HandleFunc("/tablero", tablero)

	//endpoint para peticiones
	http.HandleFunc("/loguearse", loguearse)
	http.HandleFunc("/partidaPendiente", partidaPendiente)
	http.HandleFunc("/agregarJugador", agregarJugador)
	http.HandleFunc("/tomarTiro", tomarTiro)
	http.HandleFunc("/elegirJugada", elegirJugada)
	http.HandleFunc("/tableroAdversario", tableroAdversario)
	http.HandleFunc("/tableroTerminado", tableroTerminado)
	http.HandleFunc("/terminarJuego", terminarJuego)
	http.HandleFunc("/tirar", tirar)

	//servidor
	fmt.Println("Escuchando por IP:" + ip + " PORT:" + port)
	http.ListenAndServe(":"+port, nil)
}
