var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var fs = require('fs');
var axios = require('axios');
var mysql = require('mysql');
var path = require('path');
var auth = require(path.resolve(__dirname, './authToken.js'));


import { AxiosError, AxiosResponse } from "axios";
import { Response, Request } from "express";
import { MysqlError } from "mysql";
import { Juego } from "./Logica/Juego";

var app = express();
app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));


/*Variables de entorno donde correra la API Juego*/
const ip = process.env.IP || "182.18.7.9";
const port = process.env.PORT || 4000;

/*Variables de entorno donde correra la API Simulador*/
const simip = process.env.SIMIP || "182.18.7.8";
const simport = process.env.SIMPORT || 7000;

/*Variables de entorno donde correra la API Torneo*/
const torneoip = process.env.TORNEOIP || "182.18.7.11";
const torneoport = process.env.TORNEOPORT || 8000;

/*Variables de entorno donde correra la API Usuario*/
const usersip = process.env.USERIP || "182.18.7.13";
const usersport = process.env.USERPORT || 9000;

/*Variables de entorno para la conexion con la BD*/
const DBHOST = process.env.DBHOST || "182.18.7.2";
const USER = process.env.DBUSERNAME || "root";
const PASS = process.env.DBPASSWORD || "123456789";
const DBNAME = process.env.DBNAME || "juegoDB";


//========================================= BD MYSQL =========================================//

/*Conexion con la BD MYSQL*/
var conn = mysql.createConnection({
    host: DBHOST,
    user: USER,
    password: PASS,
    database: DBNAME
});


app.post('/partidaPendiente/', (req:Request, res:Response) => {
    var jid = req.body.jid; //id del jugador logueado
    var sql = "SELECT * FROM partida WHERE (jugador1="+jid+" OR jugador2="+jid+") AND score1 = 0 AND score2 = 0;";

    conn.query(sql, function (err:MysqlError, result:Object[]) {
        if (err) res.send(err);
        res.send(JSON.stringify(result));
    });
});


//========================================== LOGIN ==========================================//

app.post('/login/', auth.authtoken, (req:Request, res:Response) => {
    var email = req.body.email;
    var pass = req.body.password;

    axios.get('http://'+usersip+':'+usersport+'/login/?email='+email+'&password='+pass)
    .then(function (response:AxiosResponse) {
        res.send(JSON.stringify(response.data));
    }).catch(function (error:AxiosError) {
        res.send(error.message);
    });
});


//====================================== JUEGO SIMULADO ======================================//

app.post('/simular', (req:Request, res:Response) => {
    var id:String = req.body.id.toString();
    var jugadores:Array<Number> = req.body.jugadores;

    if(id == undefined || jugadores == undefined){
        //uno de los dos parametros no fue recibido
        res.status(406).send("Parametros no validos");
    }else if(jugadores.length != 2){
        //el array de jugadores no coincide con lo pedido
        res.status(406).send("Parametros no validos");
    }


    axios.post('http://'+simip+':'+simport+'/simular/', {
        id: req.body.id,
        jugadores: req.body.jugadores
    }).then(function (response:AxiosResponse) {
        //obtenemos el array de scores y enviamos el score
        sendScore(response.data.resultado, id);

        var scores = response.data.resultado;

        var sql = "INSERT INTO partida(partida,jugador1,jugador2,score1,score2) VALUES('"+id+"',"+jugadores[0]+","+jugadores[1]+","+scores[0]+","+scores[1]+");";

        var resp = conn.query(sql, function (err:MysqlError, result:Object[]) {
            if(err) return err;
            return result;
        });

        res.status(201).send("Partida Simulada");
    }).catch(function (error:AxiosError) {
        //en caso de error
        res.status(404).send("Jugador no encontrado");
    });
});


//======================================== JUEGO REAL ========================================//

var arrayPartidas:Array<any> = [];
var arrayTerminadas:Array<any> = [];


//====================================================================//

app.post('/generar', (req:Request, res:Response) => {
    var id:String = req.body.id.toString();
    var jugadores:Array<Number> = req.body.jugadores;

    if(id == undefined || jugadores == undefined){
        //uno de los dos parametros no fue recibido
        res.status(406).send("Parametros no validos");
    }else if(jugadores.length != 2){
        //el array de jugadores no coincide con lo pedido
        res.status(406).send("Parametros no validos");
    }

    //[id, P1, presenteP1, P2, presenteP2, turno]
    // no presente = 0, presente = 1
    var p = [id,new Juego(jugadores[0]),0,new Juego(jugadores[1]),0,1];
    arrayPartidas.push(p);

    //[id, scoreP1, scoreP2]
    var s = [id, 0, 0];
    arrayTerminadas.push(s);


    //guardamos la partida en la BD del juego
    var sql = "INSERT INTO partida(partida,jugador1,jugador2) VALUES('"+id+"',"+jugadores[0]+","+jugadores[1]+");";

    var resp = conn.query(sql, function (err:MysqlError, result:Object[]) {
        if(err) return err;
        return result;
    });

    res.status(201).send("Partida Generada");
});


//====================================================================//

var cargarPartidasDB:boolean = false;

app.post('/agregarJugador', (req:Request, res:Response) => {
    var id  = req.body.id;  //id de la partida
    var jid = req.body.jid; //id del jugador

    
    if(!cargarPartidasDB){
        //cargar partidas pendientes ya que aun no se hizo
        var sql = "SELECT * FROM partida WHERE score1 = 0 AND score2 = 0;";

        var resp = conn.query(sql, function (err:MysqlError, result:Object[]) {
            if(!err){
                var array = JSON.parse(JSON.stringify(result));

                for(var i = 0; i < array.length; i++){
                    var id = array[i].partida.toString();
                    var jid1 = array[i].jugador1;
                    var jid2 = array[i].jugador2;

                    var p = [id,new Juego(jid1),0,new Juego(jid2),0,1];
                    arrayPartidas.push(p);
        
                    //[id, scoreP1, scoreP2]
                    var s = [id, 0, 0];
                    arrayTerminadas.push(s);
                }

                //buscamos la partida
                var index = arrayPartidas.findIndex(
                    function(object:any[]){ 
                        return String(object[0]) == id;
                    }
                );

                var resultado = true;

                if(index != -1){
                    if(arrayPartidas[index][1].getNombre() == jid){
                        //preguntamos si es el jugador 1
                        arrayPartidas[index][2] = 1;//presente
                    }else if(arrayPartidas[index][3].getNombre() == jid){
                        //preguntamos si es el jugador 2
                        arrayPartidas[index][4] = 1;//presente
                    }else{
                        resultado = false;
                    }
                }else{
                    //no encontro la partida
                    resultado = false;
                }

                //ya se cargaron las partidas
                cargarPartidasDB = true;

                res.send(JSON.stringify( {agregado: resultado} ));
            }
        });

    }else{
        //buscamos la partida
        var index = arrayPartidas.findIndex(
            function(object:any[]){ 
                return String(object[0]) == id;
            }
        );

        var resultado = true;

        if(index != -1){
            if(arrayPartidas[index][1].getNombre() == jid){
                //preguntamos si es el jugador 1
                arrayPartidas[index][2] = 1;//presente
            }else if(arrayPartidas[index][3].getNombre() == jid){
                //preguntamos si es el jugador 2
                arrayPartidas[index][4] = 1;//presente
            }else{
                resultado = false;
            }
        }else{
            //no encontro la partida
            resultado = false;
        }

        res.send(JSON.stringify( {agregado: resultado} ));
    }
});


//====================================================================//

app.post('/tomarTiro', (req:Request, res:Response) => {
    var id      = req.body.id;      //id de la partida
    var jid     = req.body.jid;     //id del jugador
    var dados   = req.body.dados;   //array de dados


    //buscamos la partida
    var index = arrayPartidas.findIndex(
        function(object:any[]){ 
            return String(object[0]) == id;
        }
    );


    //variables de control
    var log:Array<Number> = [];
    var era_turno:boolean = true;
    var comp = true;


    if(index != -1){
        var presenteP1 = arrayPartidas[index][2];
        var presenteP2 = arrayPartidas[index][4];
        var player1 = arrayPartidas[index][1];
        var player2 = arrayPartidas[index][3];
        var turno = arrayPartidas[index][5];

        if(presenteP1 == 0 || presenteP2 == 0){
            //aun no estan los 2 jugadores logueados
            log = [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1];
            comp = false;

        }else{
            if(player1.getNombre() == jid && turno == 1){
                //turno de jugador 1
                log = player1.tomarTurno(dados);
                fs.appendFile('./Log/bitacora.txt',"\t" + log + "\n", function(){});
                console.log("\t" + log + "\n");
        
            }else if(player1.getNombre() == jid && turno == 2){
                //no es turno de jugador 1
                log = [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1];
                era_turno = false;
        
            }else if(player2.getNombre() == jid && turno == 2){
                //turno de jugador 2
                log = player2.tomarTurno(dados);
                fs.appendFile('./Log/bitacora.txt',"\t" + log + "\n", function(){});
                console.log("\t" + log + "\n");
        
            }else if(player2.getNombre() == jid && turno == 1){
                //no es turno de jugador 2
                log = [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1];
                era_turno = false;

            }
        }
    }else{
        //no se encontro la partida
        log = [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1];
        era_turno = false;
        comp = false;
    }


    var json = {resultado: log, turno: era_turno, completos: comp};
    res.send(JSON.parse(JSON.stringify(json)));
});


//====================================================================//

app.post('/elegirJugada', (req:Request, res:Response) => {
    var id  = req.body.id;  //id de la partida
    var jid = req.body.jid; //id del jugador
    var pos = req.body.pos; //pos de la jugada
    var pts = req.body.pts; //puntos de la jugada


    //buscamos la partida
    var index = arrayPartidas.findIndex(
        function(object:any[]){ 
            return String(object[0]) == id;
        }
    );


    //variables de control
    var log:String = "";
    var era_turno:boolean = true;
    var comp = true;
    var ter = false;


    if(index != -1){
        var presenteP1 = arrayPartidas[index][2];
        var presenteP2 = arrayPartidas[index][4];
        var player1 = arrayPartidas[index][1];
        var player2 = arrayPartidas[index][3];
        var turno = arrayPartidas[index][5];

        if(presenteP1 == 0 || presenteP2 == 0){
            //aun no estan los 2 jugadores logueados
            log = "";
            comp = false;
    
        }else{
            if(player1.getNombre() == jid && turno == 1){
                //turno de jugador 1
                log = player1.jugadaElegida(pos, pts);
                fs.appendFile('./Log/bitacora.txt',"\t" + log + "\n", function(){});
                console.log("\t" + log + "\n");
    
                if(player1.juegoTerminado()){
                    ter = true;
                }

                if(!player2.juegoTerminado()){
                    //no a terminado la jugada aun
                    arrayPartidas[index][5] = 2;
                }

            }else if(player1.getNombre() == jid && turno == 2){
                //no es turno de jugador1
                era_turno = false;

            }else if(player2.getNombre() == jid && turno == 2){
                //turno de jugador2
                log = player2.jugadaElegida(pos, pts);
                fs.appendFile('./Log/bitacora.txt',"\t" + log + "\n", function(){});
                console.log("\t" + log + "\n");
    
                if(player2.juegoTerminado()){
                    ter = true;
                }

                if(!player1.juegoTerminado()){
                    //no a terminado la jugada aun
                    arrayPartidas[index][5] = 1;
                }

            }else if(player2.getNombre() == jid && turno == 1){
                //no es turno de jugador2
                era_turno = false;

            }
        }
    }else{
        //no encontro la partida
        log = "";
        era_turno = false;
        comp = false;
        ter = false;
    }


    var json = {resultado:log, turno:era_turno, completos:comp, termino:ter};
    res.send(JSON.parse(JSON.stringify(json)));
});


//====================================================================//

app.post('/tableroTerminado', (req:Request, res:Response) => {
    var id  = req.body.id;  //id de la partida
    var jid = req.body.jid; //id del jugador


    //buscamos la partida
    var index = arrayPartidas.findIndex(
        function(object:any[]){ 
            return String(object[0]) == id;
        }
    );

    
    //variable de control
    var log:Array<Number> = [];


    if(index != -1){
        var player1 = arrayPartidas[index][1];
        var player2 = arrayPartidas[index][3];

        if(player1.getNombre() == jid){
            log = player1.puntajeFinal(); 
            fs.appendFile('./Log/bitacora.txt',"\t" + log + "\n", function(){});
            console.log("\t" + log + "\n");
    
        }else if(player2.getNombre() == jid){
            log = player2.puntajeFinal();
            fs.appendFile('./Log/bitacora.txt',"\t" + log + "\n", function(){});
            console.log("\t" + log + "\n");
            
        }
    }


    res.send(JSON.parse(JSON.stringify( {resultado: log} )));
});


//====================================================================//

app.post('/terminarJuego', (req:Request, res:Response) => {
    var id  = req.body.id;
    var jid = req.body.jid;

    //buscamos la partida
    var index = arrayPartidas.findIndex(
        function(object:any[]){ 
            return String(object[0]) == id;
        }
    );

    //buscamos la partidat
    var indext = arrayTerminadas.findIndex(
        function(object:any[]){ 
            return String(object[0]) == id;
        }
    );

    //variable de control
    var t:boolean = false;

    if(index != -1 && indext != -1){
        var player1 = arrayPartidas[index][1];
        var player2 = arrayPartidas[index][3];

        if(player1.getNombre() == jid){
            t = true;
            fs.appendFile('./Log/bitacora.txt',"Termino: " + jid + "\n", function(){});
            console.log("Termino: " + jid + "\n");

            var score = player1.getScore();
            arrayTerminadas[indext][1] = score;
    
            if(arrayTerminadas[indext][1] > 0 && arrayTerminadas[indext][2] > 0){
                //dos ya abandonaron el tablero de juego
                var p:String  = arrayTerminadas[indext][0];
                var s1:Number = arrayTerminadas[indext][1];
                var s2:Number = arrayTerminadas[indext][2];

                //debemos guardar el score
                var sql = "UPDATE partida SET score1="+s1+",score2="+s2+" WHERE partida='"+p+"';";

                var resp = conn.query(sql, function (err:MysqlError, result:Object[]) {
                    if(err) return err;
                    return result;
                });

                //ya limpiamos la partida
                limpiar(indext);
            }
    
        }else if(player2.getNombre() == jid){
            t = true;
            fs.appendFile('./Log/bitacora.txt',"Termino: " + jid + "\n", function(){});
            console.log("Termino: " + jid + "\n");

            var score = player2.getScore();
            arrayTerminadas[indext][2] = score;

            if(arrayTerminadas[indext][1] > 0 && arrayTerminadas[indext][2] > 0){
                //dos ya abandonaron el tablero de juego
                var p:String  = arrayTerminadas[indext][0];
                var s1:Number = arrayTerminadas[indext][1];
                var s2:Number = arrayTerminadas[indext][2];

                //debemos guardar el score
                var sql = "UPDATE partida SET score1="+s1+",score2="+s2+" WHERE partida='"+p+"';";

                var resp = conn.query(sql, function (err:MysqlError, result:Object[]) {
                    if(err) return err;
                    return result;
                });

                //ya limpiamos la partida
                limpiar(indext);
            }
        }
    }


    res.send(JSON.parse(JSON.stringify( {terminado: t} )));
});


//====================================================================//

app.post('/tableroAdversario', (req:Request, res:Response) => {
    var id  = req.body.id;  //id de la partida
    var jid = req.body.jid; //id del jugador

    //buscamos la partida
    var index = arrayPartidas.findIndex(
        function(object:any[]){ 
            return String(object[0]) == id;
        }
    );

    //variable de control
    var log:Array<Number> = [];

    if(index != -1){
        var presenteP1 = arrayPartidas[index][2];
        var presenteP2 = arrayPartidas[index][4];
        var player1 = arrayPartidas[index][1];
        var player2 = arrayPartidas[index][3];

        if(presenteP1 != 0 && presenteP2 != 0){
            if(player1.getNombre() == jid){
                log = player2.getTablero();
    
                if(player2.juegoTerminado()){
                    log = player2.puntajeFinal();
                }

            }else if(player2.getNombre() == jid){
                log = player1.getTablero();

                if(player1.juegoTerminado()){
                    log = player1.puntajeFinal();
                }
            }

        }
    }


    res.send(JSON.parse(JSON.stringify( {adversario: log} )));
});


//============================================================================================//

app.listen(port, ip, async () => {
    console.log('Se escucha en el puerto: %d y con la ip: %s', port, ip);
});


//============================================================================================//

function limpiar(index:Number){
    var pid:String    = arrayTerminadas[Number(index)][0];
    var score1:Number = arrayTerminadas[Number(index)][1];
    var score2:Number = arrayTerminadas[Number(index)][2];

    //armamos el array de scores
    var arr:Array<Number> = [score1, score2];

    //enviamos la peticion al torneo
    sendScore(arr, pid);

    //eliminamos la partida de la lista
    arrayTerminadas[Number(index)][0] = "-1";
    arrayPartidas[Number(index)][0] = "-1";
}


function sendScore(arr:Array<Number>, id:String){
    console.log("Enviamos al torneo: "+arr.toString()+" Partida: "+id);
    /*axios.put('http://'+torneoip+':'+torneoport+'/partidas/'+id, {
        marcador: arr
    }).then(function (response:AxiosResponse) {
        //obtenemos el array de scores
        //debemos enviar la peticion al torneo
        return response.data;
    }).catch(function (error:AxiosError) {
        //en caso de error
        return error.message;
    });*/
}