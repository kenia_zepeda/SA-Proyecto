//require("dotenv").config();
const jwt = require("jsonwebtoken");
const fs = require('fs');
const axios = require('axios');

var publicKEY = fs.readFileSync('./public.key', 'utf8');

var verifyOptions = {
  algorithm: "RS256"
};

const scopes_validos = [
  "dados.tirar",
  "torneos.partida.get",
  "usuarios.jugadores.get"
];

const jwtip = process.env.JWTIP || "182.18.7.14";
const jwtport = process.env.JWTPORT || 10000;

/*Variables de JWT para validar los scopes*/
const jwtid = process.env.IDJWT || "b672749e-1012-11eb-adc1-0242ac120002";
const secret = process.env.SECRET || "23c485ddec558a077fd726e641766d";

async function authtoken(req, res, next){ 
  var url = 'http://'+jwtip+':'+jwtport+'/token';

  var token = await axios.post(url+"?id="+jwtid+"&secret="+secret)
  .catch(function (error){
    console.log(error);
  });

  if(token){
    var legit = jwt.verify(token.data.jwt, publicKEY, verifyOptions);

    if(legit){
      var alcanze_servicio = legit.scope;

      if(alcanze_servicio.length == scopes_validos.length &&
        !alcanze_servicio.some((v) => scopes_validos.indexOf(v) < 0)){

          console.log("Acceso concedido");
          next();
      }else{
          console.log(false);
          res.status(406).send("Acceso denegado");
      }
    }else{
      res.status(406).send("Error de token");
    }
  }else{
    console.log("Token no valido");
  }
}

module.exports.authtoken = authtoken;