"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var fs = require('fs');
var axios = require('axios');
var mysql = require('mysql');
var path = require('path');
var auth = require(path.resolve(__dirname, './authToken.js'));
const Juego_1 = require("./Logica/Juego");
var app = express();
app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
/*Variables de entorno donde correra la API Juego*/
const ip = process.env.IP || "182.18.7.9";
const port = process.env.PORT || 4000;
/*Variables de entorno donde correra la API Simulador*/
const simip = process.env.SIMIP || "182.18.7.8";
const simport = process.env.SIMPORT || 7000;
/*Variables de entorno donde correra la API Torneo*/
const torneoip = process.env.TORNEOIP || "182.18.7.11";
const torneoport = process.env.TORNEOPORT || 8000;
/*Variables de entorno donde correra la API Usuario*/
const usersip = process.env.USERIP || "182.18.7.13";
const usersport = process.env.USERPORT || 9000;
/*Variables de entorno para la conexion con la BD*/
const DBHOST = process.env.DBHOST || "182.18.7.2";
const USER = process.env.DBUSERNAME || "root";
const PASS = process.env.DBPASSWORD || "123456789";
const DBNAME = process.env.DBNAME || "juegoDB";
//========================================= BD MYSQL =========================================//
/*Conexion con la BD MYSQL*/
var conn = mysql.createConnection({
    host: DBHOST,
    user: USER,
    password: PASS,
    database: DBNAME
});
app.post('/partidaPendiente/', (req, res) => {
    var jid = req.body.jid; //id del jugador logueado
    var sql = "SELECT * FROM partida WHERE (jugador1=" + jid + " OR jugador2=" + jid + ") AND score1 = 0 AND score2 = 0;";
    conn.query(sql, function (err, result) {
        if (err)
            res.send(err);
        res.send(JSON.stringify(result));
    });
});
//========================================== LOGIN ==========================================//
app.post('/login/', auth.authtoken, (req, res) => {
    var email = req.body.email;
    var pass = req.body.password;
    axios.get('http://' + usersip + ':' + usersport + '/login/?email=' + email + '&password=' + pass)
        .then(function (response) {
        res.send(JSON.stringify(response.data));
    }).catch(function (error) {
        res.send(error.message);
    });
});
//====================================== JUEGO SIMULADO ======================================//
app.post('/simular', (req, res) => {
    var id = req.body.id.toString();
    var jugadores = req.body.jugadores;
    if (id == undefined || jugadores == undefined) {
        //uno de los dos parametros no fue recibido
        res.status(406).send("Parametros no validos");
    }
    else if (jugadores.length != 2) {
        //el array de jugadores no coincide con lo pedido
        res.status(406).send("Parametros no validos");
    }
    axios.post('http://' + simip + ':' + simport + '/simular/', {
        id: req.body.id,
        jugadores: req.body.jugadores
    }).then(function (response) {
        //obtenemos el array de scores y enviamos el score
        sendScore(response.data.resultado, id);
        var scores = response.data.resultado;
        var sql = "INSERT INTO partida(partida,jugador1,jugador2,score1,score2) VALUES('" + id + "'," + jugadores[0] + "," + jugadores[1] + "," + scores[0] + "," + scores[1] + ");";
        var resp = conn.query(sql, function (err, result) {
            if (err)
                return err;
            return result;
        });
        res.status(201).send("Partida Simulada");
    }).catch(function (error) {
        //en caso de error
        res.status(404).send("Jugador no encontrado");
    });
});
//======================================== JUEGO REAL ========================================//
var arrayPartidas = [];
var arrayTerminadas = [];
//====================================================================//
app.post('/generar', (req, res) => {
    var id = req.body.id.toString();
    var jugadores = req.body.jugadores;
    if (id == undefined || jugadores == undefined) {
        //uno de los dos parametros no fue recibido
        res.status(406).send("Parametros no validos");
    }
    else if (jugadores.length != 2) {
        //el array de jugadores no coincide con lo pedido
        res.status(406).send("Parametros no validos");
    }
    //[id, P1, presenteP1, P2, presenteP2, turno]
    // no presente = 0, presente = 1
    var p = [id, new Juego_1.Juego(jugadores[0]), 0, new Juego_1.Juego(jugadores[1]), 0, 1];
    arrayPartidas.push(p);
    //[id, scoreP1, scoreP2]
    var s = [id, 0, 0];
    arrayTerminadas.push(s);
    //guardamos la partida en la BD del juego
    var sql = "INSERT INTO partida(partida,jugador1,jugador2) VALUES('" + id + "'," + jugadores[0] + "," + jugadores[1] + ");";
    var resp = conn.query(sql, function (err, result) {
        if (err)
            return err;
        return result;
    });
    res.status(201).send("Partida Generada");
});
//====================================================================//
var cargarPartidasDB = false;
app.post('/agregarJugador', (req, res) => {
    var id = req.body.id; //id de la partida
    var jid = req.body.jid; //id del jugador
    if (!cargarPartidasDB) {
        //cargar partidas pendientes ya que aun no se hizo
        var sql = "SELECT * FROM partida WHERE score1 = 0 AND score2 = 0;";
        var resp = conn.query(sql, function (err, result) {
            if (!err) {
                var array = JSON.parse(JSON.stringify(result));
                for (var i = 0; i < array.length; i++) {
                    var id = array[i].partida.toString();
                    var jid1 = array[i].jugador1;
                    var jid2 = array[i].jugador2;
                    var p = [id, new Juego_1.Juego(jid1), 0, new Juego_1.Juego(jid2), 0, 1];
                    arrayPartidas.push(p);
                    //[id, scoreP1, scoreP2]
                    var s = [id, 0, 0];
                    arrayTerminadas.push(s);
                }
                //buscamos la partida
                var index = arrayPartidas.findIndex(function (object) {
                    return String(object[0]) == id;
                });
                var resultado = true;
                if (index != -1) {
                    if (arrayPartidas[index][1].getNombre() == jid) {
                        //preguntamos si es el jugador 1
                        arrayPartidas[index][2] = 1; //presente
                    }
                    else if (arrayPartidas[index][3].getNombre() == jid) {
                        //preguntamos si es el jugador 2
                        arrayPartidas[index][4] = 1; //presente
                    }
                    else {
                        resultado = false;
                    }
                }
                else {
                    //no encontro la partida
                    resultado = false;
                }
                //ya se cargaron las partidas
                cargarPartidasDB = true;
                res.send(JSON.stringify({ agregado: resultado }));
            }
        });
    }
    else {
        //buscamos la partida
        var index = arrayPartidas.findIndex(function (object) {
            return String(object[0]) == id;
        });
        var resultado = true;
        if (index != -1) {
            if (arrayPartidas[index][1].getNombre() == jid) {
                //preguntamos si es el jugador 1
                arrayPartidas[index][2] = 1; //presente
            }
            else if (arrayPartidas[index][3].getNombre() == jid) {
                //preguntamos si es el jugador 2
                arrayPartidas[index][4] = 1; //presente
            }
            else {
                resultado = false;
            }
        }
        else {
            //no encontro la partida
            resultado = false;
        }
        res.send(JSON.stringify({ agregado: resultado }));
    }
});
//====================================================================//
app.post('/tomarTiro', (req, res) => {
    var id = req.body.id; //id de la partida
    var jid = req.body.jid; //id del jugador
    var dados = req.body.dados; //array de dados
    //buscamos la partida
    var index = arrayPartidas.findIndex(function (object) {
        return String(object[0]) == id;
    });
    //variables de control
    var log = [];
    var era_turno = true;
    var comp = true;
    if (index != -1) {
        var presenteP1 = arrayPartidas[index][2];
        var presenteP2 = arrayPartidas[index][4];
        var player1 = arrayPartidas[index][1];
        var player2 = arrayPartidas[index][3];
        var turno = arrayPartidas[index][5];
        if (presenteP1 == 0 || presenteP2 == 0) {
            //aun no estan los 2 jugadores logueados
            log = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1];
            comp = false;
        }
        else {
            if (player1.getNombre() == jid && turno == 1) {
                //turno de jugador 1
                log = player1.tomarTurno(dados);
                fs.appendFile('./Log/bitacora.txt', "\t" + log + "\n", function () { });
                console.log("\t" + log + "\n");
            }
            else if (player1.getNombre() == jid && turno == 2) {
                //no es turno de jugador 1
                log = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1];
                era_turno = false;
            }
            else if (player2.getNombre() == jid && turno == 2) {
                //turno de jugador 2
                log = player2.tomarTurno(dados);
                fs.appendFile('./Log/bitacora.txt', "\t" + log + "\n", function () { });
                console.log("\t" + log + "\n");
            }
            else if (player2.getNombre() == jid && turno == 1) {
                //no es turno de jugador 2
                log = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1];
                era_turno = false;
            }
        }
    }
    else {
        //no se encontro la partida
        log = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1];
        era_turno = false;
        comp = false;
    }
    var json = { resultado: log, turno: era_turno, completos: comp };
    res.send(JSON.parse(JSON.stringify(json)));
});
//====================================================================//
app.post('/elegirJugada', (req, res) => {
    var id = req.body.id; //id de la partida
    var jid = req.body.jid; //id del jugador
    var pos = req.body.pos; //pos de la jugada
    var pts = req.body.pts; //puntos de la jugada
    //buscamos la partida
    var index = arrayPartidas.findIndex(function (object) {
        return String(object[0]) == id;
    });
    //variables de control
    var log = "";
    var era_turno = true;
    var comp = true;
    var ter = false;
    if (index != -1) {
        var presenteP1 = arrayPartidas[index][2];
        var presenteP2 = arrayPartidas[index][4];
        var player1 = arrayPartidas[index][1];
        var player2 = arrayPartidas[index][3];
        var turno = arrayPartidas[index][5];
        if (presenteP1 == 0 || presenteP2 == 0) {
            //aun no estan los 2 jugadores logueados
            log = "";
            comp = false;
        }
        else {
            if (player1.getNombre() == jid && turno == 1) {
                //turno de jugador 1
                log = player1.jugadaElegida(pos, pts);
                fs.appendFile('./Log/bitacora.txt', "\t" + log + "\n", function () { });
                console.log("\t" + log + "\n");
                if (player1.juegoTerminado()) {
                    ter = true;
                }
                if (!player2.juegoTerminado()) {
                    //no a terminado la jugada aun
                    arrayPartidas[index][5] = 2;
                }
            }
            else if (player1.getNombre() == jid && turno == 2) {
                //no es turno de jugador1
                era_turno = false;
            }
            else if (player2.getNombre() == jid && turno == 2) {
                //turno de jugador2
                log = player2.jugadaElegida(pos, pts);
                fs.appendFile('./Log/bitacora.txt', "\t" + log + "\n", function () { });
                console.log("\t" + log + "\n");
                if (player2.juegoTerminado()) {
                    ter = true;
                }
                if (!player1.juegoTerminado()) {
                    //no a terminado la jugada aun
                    arrayPartidas[index][5] = 1;
                }
            }
            else if (player2.getNombre() == jid && turno == 1) {
                //no es turno de jugador2
                era_turno = false;
            }
        }
    }
    else {
        //no encontro la partida
        log = "";
        era_turno = false;
        comp = false;
        ter = false;
    }
    var json = { resultado: log, turno: era_turno, completos: comp, termino: ter };
    res.send(JSON.parse(JSON.stringify(json)));
});
//====================================================================//
app.post('/tableroTerminado', (req, res) => {
    var id = req.body.id; //id de la partida
    var jid = req.body.jid; //id del jugador
    //buscamos la partida
    var index = arrayPartidas.findIndex(function (object) {
        return String(object[0]) == id;
    });
    //variable de control
    var log = [];
    if (index != -1) {
        var player1 = arrayPartidas[index][1];
        var player2 = arrayPartidas[index][3];
        if (player1.getNombre() == jid) {
            log = player1.puntajeFinal();
            fs.appendFile('./Log/bitacora.txt', "\t" + log + "\n", function () { });
            console.log("\t" + log + "\n");
        }
        else if (player2.getNombre() == jid) {
            log = player2.puntajeFinal();
            fs.appendFile('./Log/bitacora.txt', "\t" + log + "\n", function () { });
            console.log("\t" + log + "\n");
        }
    }
    res.send(JSON.parse(JSON.stringify({ resultado: log })));
});
//====================================================================//
app.post('/terminarJuego', (req, res) => {
    var id = req.body.id;
    var jid = req.body.jid;
    //buscamos la partida
    var index = arrayPartidas.findIndex(function (object) {
        return String(object[0]) == id;
    });
    //buscamos la partidat
    var indext = arrayTerminadas.findIndex(function (object) {
        return String(object[0]) == id;
    });
    //variable de control
    var t = false;
    if (index != -1 && indext != -1) {
        var player1 = arrayPartidas[index][1];
        var player2 = arrayPartidas[index][3];
        if (player1.getNombre() == jid) {
            t = true;
            fs.appendFile('./Log/bitacora.txt', "Termino: " + jid + "\n", function () { });
            console.log("Termino: " + jid + "\n");
            var score = player1.getScore();
            arrayTerminadas[indext][1] = score;
            if (arrayTerminadas[indext][1] > 0 && arrayTerminadas[indext][2] > 0) {
                //dos ya abandonaron el tablero de juego
                var p = arrayTerminadas[indext][0];
                var s1 = arrayTerminadas[indext][1];
                var s2 = arrayTerminadas[indext][2];
                //debemos guardar el score
                var sql = "UPDATE partida SET score1=" + s1 + ",score2=" + s2 + " WHERE partida='" + p + "';";
                var resp = conn.query(sql, function (err, result) {
                    if (err)
                        return err;
                    return result;
                });
                //ya limpiamos la partida
                limpiar(indext);
            }
        }
        else if (player2.getNombre() == jid) {
            t = true;
            fs.appendFile('./Log/bitacora.txt', "Termino: " + jid + "\n", function () { });
            console.log("Termino: " + jid + "\n");
            var score = player2.getScore();
            arrayTerminadas[indext][2] = score;
            if (arrayTerminadas[indext][1] > 0 && arrayTerminadas[indext][2] > 0) {
                //dos ya abandonaron el tablero de juego
                var p = arrayTerminadas[indext][0];
                var s1 = arrayTerminadas[indext][1];
                var s2 = arrayTerminadas[indext][2];
                //debemos guardar el score
                var sql = "UPDATE partida SET score1=" + s1 + ",score2=" + s2 + " WHERE partida='" + p + "';";
                var resp = conn.query(sql, function (err, result) {
                    if (err)
                        return err;
                    return result;
                });
                //ya limpiamos la partida
                limpiar(indext);
            }
        }
    }
    res.send(JSON.parse(JSON.stringify({ terminado: t })));
});
//====================================================================//
app.post('/tableroAdversario', (req, res) => {
    var id = req.body.id; //id de la partida
    var jid = req.body.jid; //id del jugador
    //buscamos la partida
    var index = arrayPartidas.findIndex(function (object) {
        return String(object[0]) == id;
    });
    //variable de control
    var log = [];
    if (index != -1) {
        var presenteP1 = arrayPartidas[index][2];
        var presenteP2 = arrayPartidas[index][4];
        var player1 = arrayPartidas[index][1];
        var player2 = arrayPartidas[index][3];
        if (presenteP1 != 0 && presenteP2 != 0) {
            if (player1.getNombre() == jid) {
                log = player2.getTablero();
                if (player2.juegoTerminado()) {
                    log = player2.puntajeFinal();
                }
            }
            else if (player2.getNombre() == jid) {
                log = player1.getTablero();
                if (player1.juegoTerminado()) {
                    log = player1.puntajeFinal();
                }
            }
        }
    }
    res.send(JSON.parse(JSON.stringify({ adversario: log })));
});
//============================================================================================//
app.listen(port, ip, () => __awaiter(void 0, void 0, void 0, function* () {
    console.log('Se escucha en el puerto: %d y con la ip: %s', port, ip);
}));
//============================================================================================//
function limpiar(index) {
    var pid = arrayTerminadas[Number(index)][0];
    var score1 = arrayTerminadas[Number(index)][1];
    var score2 = arrayTerminadas[Number(index)][2];
    //armamos el array de scores
    var arr = [score1, score2];
    //enviamos la peticion al torneo
    sendScore(arr, pid);
    //eliminamos la partida de la lista
    arrayTerminadas[Number(index)][0] = "-1";
    arrayPartidas[Number(index)][0] = "-1";
}
function sendScore(arr, id) {
    console.log("Enviamos al torneo: " + arr.toString() + " Partida: " + id);
    /*axios.put('http://'+torneoip+':'+torneoport+'/partidas/'+id, {
        marcador: arr
    }).then(function (response:AxiosResponse) {
        //obtenemos el array de scores
        //debemos enviar la peticion al torneo
        return response.data;
    }).catch(function (error:AxiosError) {
        //en caso de error
        return error.message;
    });*/
}
