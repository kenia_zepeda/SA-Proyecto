
function initGame(){
    var logeado = sessionStorage.getItem('logueado');
    if(!logeado){
        window.location.href = "../";
    }

    //mostrar el nombre del usuario
    var nombre = sessionStorage.getItem('nombre-usuario');
    document.getElementById("nombre_user").innerText = nombre;

    //limpiamos los campos
    limpiar();

    //cargamos los dropdown
    fetch('../listaJuego',{
        method: 'GET'
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => viewGames(response));
}

function regresar(){
    window.location.href = "../index";
}


//==================================== SELECT ====================================//

function viewGames(response){
    //suponiendo que es un array de juegos en formato json
    //var array = JSON.parse(JSON.stringify(response));

    var array = [
        {"nombre":"juego1","id":12345,"url":"http://..."},
        {"nombre":"juego2","id":12445,"url":"http://..."},
        {"nombre":"juego3","id":12845,"url":"http://..."},
        {"nombre":"juego4","id":12745,"url":"http://..."},
        {"nombre":"juego5","id":12245,"url":"http://..."},
        {"nombre":"juego6","id":12045,"url":"http://..."},
    ]

    //lista de juegos para modificar
    var upd = document.getElementById("juego_m");

    //lista de juegos para eliminar
    var del = document.getElementById("juego_e");

    array.forEach(element => {
        var btnu = document.createElement("button");
        btnu.setAttribute('id',element.id);
        btnu.setAttribute('name',element.url);
        btnu.setAttribute('onclick','selectGame(this);');
        btnu.setAttribute("class","dropdown-item");
        btnu.innerText = element.nombre;

        upd.appendChild(btnu);

        var btnd = document.createElement("button");
        btnd.setAttribute('id',element.id);
        btnd.setAttribute('name',element.url);
        btnd.setAttribute('onclick','deleteGame(this);');
        btnd.setAttribute("class","dropdown-item");
        btnd.innerText = element.nombre;

        del.appendChild(btnd);
    });
}


//===================================== CREAR =====================================//

function addGame(){
    var nombre = document.getElementById("a-nombre").value;
    var link = document.getElementById("a-link").value;

    if(nombre != "" && link != ""){
        fetch('../insertJuego',{
            method: 'POST',
            body: JSON.stringify({"Jid":0,"Name":nombre,"URL":link}),
            headers:{
              'Content-Type': 'application/json'
            }
        }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response) /*limpiar()*/);
    }
}


//=================================== MODIFICAR ===================================//

function selectGame(btn){
    //id del juego
    document.getElementById("m-id").innerText = btn.id;
    //nombre del juego
    document.getElementById("m-nombre").value = btn.innerText;
    //link del juego
    document.getElementById("m-link").value = btn.name;
}

function updateGame(){
    var id = document.getElementById("m-id").innerText;
    var nombre = document.getElementById("m-nombre").value;
    var link = document.getElementById("m-link").value;

    fetch('../updateJuego',{
        method: 'POST',
        body: JSON.stringify({"Jid":id,"Name":nombre,"URL":link}),
        headers:{
          'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response) /*location.reload()*/);
}


//==================================== ELIMINAR ====================================//

function deleteGame(btn){
    var id = btn.id;

    fetch('../deleteJuego',{
        method: 'POST',
        body: JSON.stringify({"Jid":id,"Name":"","URL":""}),
        headers:{
          'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response) /*location.reload()*/);
}


//==================================== LIMPIAR ====================================//

function limpiar(){
    document.getElementById("a-nombre").innerText = '';
    document.getElementById("a-link").innerText = '';

    document.getElementById("m-id").style.visibility = "hidden";
    document.getElementById("m-id").innerText = '';
    document.getElementById("m-nombre").innerText = '';
    document.getElementById("m-link").innerText = '';
}