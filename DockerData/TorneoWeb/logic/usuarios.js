
function initUser(){
    var logeado = sessionStorage.getItem('logueado');
    if(!logeado){
        window.location.href = "../";
    }

    //mostrar el nombre del usuario
    var nombre = sessionStorage.getItem('nombre-usuario');
    document.getElementById("nombre_user").innerText = nombre;

    //limpiamos los campos
    limpiar();

    //cargamos los dropdown
    fetch('../listaUsuario',{
        method: 'GET'
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => viewUser(response));
}

function regresar(){
    window.location.href = "../index";
}


//==================================== SELECT ====================================//

var array_users_info = [];

function viewUser(response){
    //suponiendo que es un array de usuarios en formato json
    //var array = JSON.parse(JSON.stringify(response));

    var array = [
        {"nombres":"nombre1","id":12345},//se tendra toda la informacion
        {"nombres":"nombre2","id":12445},
        {"nombres":"nombre3","id":12845},
        {"nombres":"nombre4","id":12745},
        {"nombres":"nombre5","id":12245},
        {"nombres":"nombre6","id":12045},
    ]

    //guardamos la info de los usuarios
    array_users_info = array;

    //lista de usuarios para modificar
    var upd = document.getElementById("users_m");

    //lista de usuarios para eliminar
    var del = document.getElementById("users_e");

    array.forEach(element => {
        var btnu = document.createElement("button");
        btnu.setAttribute('id',element.id);
        btnu.setAttribute('onclick','selectUser(this);');
        btnu.setAttribute("class","dropdown-item");
        btnu.innerText = element.nombres;

        upd.appendChild(btnu);

        var btnd = document.createElement("button");
        btnd.setAttribute('id',element.id);
        btnd.setAttribute('onclick','deleteUser(this);');
        btnd.setAttribute("class","dropdown-item");
        btnd.innerText = element.nombres;

        del.appendChild(btnd);
    });
}


//===================================== CREAR =====================================//

function addUser(){
    var email = document.getElementById("a-email").value;
    var nombre = document.getElementById("a-nombre").value;
    var apellido = document.getElementById("a-apellido").value;
    var pass = document.getElementById("a-pass").value;
    var admin = document.getElementById("a-admin").checked;

    if(nombre != "" && apellido != "" && pass != "" && email != ""){
        var adm = 0;
        if(admin) adm = 1;

        fetch('../insertUsuario',{
            method: 'POST',
            body: JSON.stringify({
                "UID":0,
                "Nombre":nombre,
                "Apellido":apellido,
                "Email":email,
                "Pass":pass,
                "Admin":adm
            }),
            headers:{
              'Content-Type': 'application/json'
            }
        }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response) /*limpiar()*/);
    }
}


//=================================== MODIFICAR ===================================//

function selectUser(btn){
    var index = array_users_info.findIndex(
        function(element){ 
            return element.id == btn.id;
        }
    );

    //encontramos el usuario pedido
    var user = array_users_info[index];

    document.getElementById("m-id").innerText = btn.id;
    document.getElementById("m-email").value = user.email;
    document.getElementById("m-nombre").value = user.nombres;
    document.getElementById("m-apellido").value = user.apellidos;
    document.getElementById("m-pass").value = user.password;

    var check = false;
    if(user.administrador == 1) check = true;

    document.getElementById("m-admin").checked = check;
}

function updateUser(){
    var id = document.getElementById("m-id").innerText;
    var email = document.getElementById("m-email").value;
    var nombre = document.getElementById("m-nombre").value;
    var apellido = document.getElementById("m-apellido").value;
    var pass = document.getElementById("m-pass").value;
    var admin = document.getElementById("m-admin").checked;

    var adm = 0;
    if(admin) adm = 1;

    fetch('../updateUsuario',{
        method: 'POST',
        body: JSON.stringify({
            "UID":id,
            "Nombre":nombre,
            "Apellido":apellido,
            "Email":email,
            "Pass":pass,
            "Admin":adm
        }),
        headers:{
          'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response) /*location.reload()*/);
}


//==================================== ELIMINAR ====================================//

function deleteUser(btn){
    var id = btn.id;

    fetch('../deleteUsuario',{
        method: 'POST',
        body: JSON.stringify({
            "UID":id,
            "Nombre":"",
            "Apellido":"",
            "Email":"",
            "Pass":"",
            "Admin":""
        }),
        headers:{
          'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response) /*location.reload()*/);
}


//==================================== LIMPIAR ====================================//

function limpiar(){
    document.getElementById("a-email").innerText = '';
    document.getElementById("a-nombre").innerText = '';
    document.getElementById("a-apellido").innerText = '';
    document.getElementById("a-pass").innerText = '';
    document.getElementById("a-admin").checked = false;

    document.getElementById("m-id").style.visibility = "hidden";
    document.getElementById("m-id").innerText = '';

    document.getElementById("m-email").innerText = '';
    document.getElementById("m-nombre").innerText = '';
    document.getElementById("m-apellido").innerText = '';
    document.getElementById("m-pass").innerText = '';
    document.getElementById("m-admin").checked = false;
}