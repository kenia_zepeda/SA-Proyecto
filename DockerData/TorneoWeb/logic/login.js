
function loguearse(){
    var email = document.getElementById("mail").value;
    var pass = document.getElementById("pass").value;

    if(email!="" && pass!=""){
        fetch('../loguearse',{
            method: 'POST',
            body: JSON.stringify({"Email":email,"Pass":pass}),
            headers:{
              'Content-Type': 'application/json'
            }
        }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => logueado(response));
        
    }else{
        alert("Ingrese todos los campos");
    }
}

function logueado(response){
    sessionStorage.setItem('nombre-usuario', "Nombre");
    sessionStorage.setItem('logueado', true);
    window.location.href = "../index";
    /*
    if(response != undefined){
        var user = JSON.parse(JSON.stringify(response));
        sessionStorage.setItem('id-usuario', user.id);
        sessionStorage.setItem('nombre-usuario', user.nombres);
        sessionStorage.setItem('logueado', true);
        window.location.href = "../index";
    }else{
        alert("Credenciales Incorrectas");
    }*/
}