
function initTorneo(){
    var logeado = sessionStorage.getItem('logueado');
    if(!logeado){
        window.location.href = "../";
    }

    //mostrar el nombre del usuario
    var nombre = sessionStorage.getItem('nombre-usuario');
    document.getElementById("nombre_user").innerText = nombre;


    //llenamos el dropdown de juegos
    fetch('../listaJuego',{
        method: 'GET'
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => viewGames(response));

    //llenamos el dropdown de jugadores
    fetch('../listaUsuario',{
        method: 'GET'
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => viewUser(response));
}

function usuarios(){
    window.location.href = "../usuarios";
}

function juegos(){
    window.location.href = "../juegos";
}

function logout(){
    sessionStorage.clear();
    window.location.href = "../";
}


//==================================== LISTA USUARIOS ====================================//

function viewUser(response){
    //suponiendo que es un array de usuarios en formato json
    //var array = JSON.parse(JSON.stringify(response));
    var array = [
        {"nombre":"nombre1","id":12345},
        {"nombre":"nombre2","id":12445},
        {"nombre":"nombre3","id":12845},
        {"nombre":"nombre4","id":12745},
        {"nombre":"nombre5","id":12245},
        {"nombre":"nombre6","id":12045},
    ]

    var jugadores = document.getElementById("jugador-lista");

    array.forEach(element => {

        var btn = document.createElement("button");
        btn.setAttribute('id',element.id);
        btn.setAttribute('onclick','addUser(this);');
        btn.setAttribute("class","dropdown-item");
        btn.innerText = element.nombre;

        jugadores.appendChild(btn);
    });
}

var array_user = [];

function addUser(btn){
    var iduser = btn.id;
    btn.disabled = true;
    array_user.push(iduser);

    var num = document.getElementById("num-players");
    num.innerText = "Participantes: " + array_user.length;


    console.log("agregaste: "+iduser+" "+btn.value);
}


//==================================== LISTA JUEGOS ====================================//

function viewGames(response){
    //suponiendo que es un array de juegos en formato json
    //var array = JSON.parse(JSON.stringify(response));

    var array = [
        {"nombre":"juego1","id":12345,"url":"http://..."},
        {"nombre":"juego2","id":12445,"url":"http://..."},
        {"nombre":"juego3","id":12845,"url":"http://..."},
        {"nombre":"juego4","id":12745,"url":"http://..."},
        {"nombre":"juego5","id":12245,"url":"http://..."},
        {"nombre":"juego6","id":12045,"url":"http://..."},
    ]

    var dropdown = document.getElementById("juegos-lista");

    array.forEach(element => {
        var btn = document.createElement("button");
        btn.setAttribute('id',element.id);
        btn.setAttribute('name',element.url);
        btn.setAttribute('onclick','addGame(this);');
        btn.setAttribute("class","dropdown-item");
        btn.innerText = element.nombre;

        dropdown.appendChild(btn);
    });
}

var urlgame = "";
var idgame = "";

function addGame(btn){
    urlgame = btn.name;
    idgame = btn.id;

    var select = document.getElementById("game-select");
    select.innerText = "Juego Elegido: " + btn.innerText;


    console.log("Escogiste: "+btn.value+" "+idgame);
}



function sendTorneo(){
    array_user = array_user.toString();

    fetch('../CrearTorneo', {
        method: 'POST',
        body: JSON.stringify({"Jid":idgame,"URL":urlgame,"Jugadores":array_user}),
        headers:{
          'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => viewUser(response));
}