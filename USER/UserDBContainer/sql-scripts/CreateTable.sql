CREATE DATABASE usuarioDB;
use usuarioDB;
CREATE TABLE IF NOT EXISTS usuario(
  id INTEGER NOT NULL AUTO_INCREMENT,
  nombres VARCHAR(500),
  apellidos VARCHAR(500),
  email VARCHAR(500),
  password VARCHAR(500),
  administrador TINYINT(1),

  PRIMARY KEY (id)
);