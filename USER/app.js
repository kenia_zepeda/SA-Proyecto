require("dotenv").config();
'use strict';
const fs   = require('fs');
const express = require("express");
var cookieParser = require('cookie-parser');
const bodyparser = require('body-parser');

const app = express();
/*IMPORTACION DE RUTAS */
var loginRouter = require('./routes/jugador');
var jugadorRouter = require('./routes/login');

app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/login', jugadorRouter);
app.use('/jugadores', loginRouter);


app.get("/", (req, res) => {
    res.send("SERVIDOR USUARIOS ENCENDIDO");
  });

  app.listen(process.env.PORT,function(error){ 
    if (error) throw error 
    console.log("Server created Successfully on PORT", process.env.PORT) 
});