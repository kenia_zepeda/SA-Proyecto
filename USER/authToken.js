require("dotenv").config();
const jwt = require("jsonwebtoken");
const fs   = require('fs');
const axios = require('axios');

//lleve publica para poder descriptar los jwt enviador al servidor
var publicKEY  = fs.readFileSync('./public.key', 'utf8');
/*Aqui tenemos las opciones de verificacion del token */ 
var verifyOptions = {
  algorithm:  "RS256"
 };
 //scopes validos dentro del servicio
const scopes_validos = ['usuarios.login','usuarios.jugadores.get','usuarios.jugadores.post','usuarios.jugadores.put'];
async function authtoken(req, res, next){
  /*url del servicio de jwt*/ 
  var url = 'http://'+process.env.IP_JWT+':'+process.env.PORT_JWT+'/token';
  //hacemos el post al sevidor de token para obtener el token encriptado usando el id y el secret
  var token = await axios.post(url+"?id="+process.env.ID_JWT+"&secret="+process.env.SECRET).catch(function (error) {
    console.log(error);
  });
  if (token){
    //descriptar el token usando la llave publica
    var legit = jwt.verify(token.data.jwt, publicKEY, verifyOptions);
  //vemos el valor desencriptado en json y ver su data
  //aqui tomamos el valor del token desencriptador y ver cual es el scope segun el microservicio
    if (legit){
      var alcanze_servicio= legit.scope
      //comparamos los elementos del scope y los que tenemos validos
      if(alcanze_servicio.length == scopes_validos.length && !alcanze_servicio.some((v) => scopes_validos.indexOf(v) < 0)) {
        console.log("El servicio puede realizar su operacion normal");
        next()

    } else {
        console.log(false);
        res.status(406).send("El servicio no tiene permisos para realizar esta operacion!!")
    }
    }else{
      res.status(406).send("Error de token!!")
    }
   

  }else{
    console.log("TOKEN NO VALIDO!!")
  }
  
  
      
  
}

module.exports.authtoken = authtoken;