var express = require("express");
var router = express.Router();
var connection = require('../connection');

var auth = require('../authToken');
//obtiene todos los jugadores que estan en la base de datos
router.get("/jugadores",auth.authtoken, (req, res)=>{
    var sql = `SELECT id,email,nombres,apellidos,administrador FROM usuario`;
    connection.query(sql, function (err, result, fields) {
        if (err) res.sendStatus(404);
        if(result.length!=0){
           res.status(200).json(result)
        }else{
            res.status(200).send("No hay jugadore disponibles!!")
        }
    });
});
// ruta GET /jugadores/:id 
router.get("/:id", auth.authtoken, (req, res) => {
    
    var user_id = req.params['id']
    if(user_id!=null){
    var sql = `SELECT * FROM usuario WHERE usuario.id='${user_id}'`;
    connection.query(sql, function (err, result, fields) {
            if (err) throw err;
            if (result.length==0){
                res.sendStatus(404);
            }else{
                if (result[0].administrador==0){
                    result[0].administrador= false;
                }else{
                    result[0].administrador= true;
                }
                res.status(200).json(result[0]);
            }
        });
    }else{
        res.status(404).send("Error en el id de jugador!!")
    }
    
  });
// ruta PUT /jugadores/:id 
router.put("/:id",auth.authtoken, (req, res) => {
    var user_id = req.params['id']
    var sql = `SELECT * FROM usuario WHERE usuario.id='${user_id}'`;
    connection.query(sql, function (err, result, fields) {
        if (err){ res.sendStatus(406);}

        if (result.length==0){
            res.sendStatus(404);
        }else{
            var admin =0;
            //creamos la conexion a la bd
                //si el administrador es true cambiamos el valor por 1 sino sera 0
            if (req.body.administrador==true){
                 admin =1;
            }
            var sql = `UPDATE usuario SET nombres='${req.body.nombres}', apellidos='${req.body.apellidos}',email='${req.body.email}',password='${req.body.password}', administrador='${admin}' WHERE id='${user_id}'`;
            connection.query(sql, function (err, result, fields) {
                if (err) res.sendStatus(406);
                if (result.length==0){
                    res.sendStatus(404);
                }else{
                    if (admin==1){
                        admin=true;
                    }else{
                        admin=false;
                    }
                    res.status(201).json({
                        id: user_id,
                        nombres: req.body.nombres,
                        apellidos: req.body.apellidos,
                        email: req.body.email,
                        password: req.body.password,
                        administrador: admin});
                
                }


                });
            }    
    });
  });
// ruta POST /jugadores/ 
router.post("/",auth.authtoken, (req, res) => {
    var admin =0;
    //creamos la conexion a la bd
        //si el administrador es true cambiamos el valor por 1 sino sera 0
    if (req.body.administrador==true){
         admin =1;
    }
        // creamos el sql para insertar a la base e datos
    var sql = `INSERT INTO usuario (nombres,apellidos,email,password,administrador) VALUES ('${req.body.nombres}','${req.body.apellidos}','${req.body.email}','${req.body.password}','${admin}')`;
    connection.query(sql, function (err, result) {
            //insertar no valida mandamos erro 406
        if (err) res.sendStatus(406);
            //regresmos el json con lo pedido en el swagger
        if (admin==1){
            admin=true;
        }else{
            admin=false;
        }               
        res.status(201).json({
                id: result.insertId,
                nombres: req.body.nombres,
                apellidos: req.body.apellidos,
                email: req.body.email,
                password: req.body.password,
                administrador: admin  

        })
    });
      
   
  });

module.exports = router;