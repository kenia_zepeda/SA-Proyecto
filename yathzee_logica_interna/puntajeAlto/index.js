"use strict";

// Sort array.
const compare = (a, b) => {
    if (a.score > b.score) return -1;
    if (a.score < b.score) return 1;
    return 0;
};
module.exports = {
    // ordenar puntaje
    ordenarPuntajesAltos: puntajes => {
        return puntajes.sort(compare);
    },

    // Save high score. Push to array.
    setPuntajeAlto: (puntajes, puntajeAlto) => {
        puntajes.push(puntajeAlto);
        return puntajes;
    }
};