"use strict";
module.exports = {
    execute: (juegoId, juegos) => {
        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth() + 1; //January is 0!
        let yyyy = today.getFullYear();
        if (dd < 10) {
            dd = "0" + dd;
        }
        if (mm < 10) {
            mm = "0" + mm;
        }
        today = dd + "." + mm + "." + yyyy;
        let index = juegos
            .map(o => {
                return o.juegoId;
            })
            .indexOf(juegoId);

        // finalizar juego y establecer puntuación más alta
        juegos[index].ronda = 16;
        juegos[index].lanzamiento = 4;
        let puntajeAlto = {
            name: juegos[index].nombreJugador,
            score: juegos[index].total,
            date: today
        };
        juegos[index].puntajeAlto = puntajeAlto;
        return juegos;
    }
};

