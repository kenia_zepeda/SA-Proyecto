"use strict";
module.exports = {

    execute: (juegoId, juegos) => {
        let index = juegos.map(
            o => {
                return o.juegoId;
            })
            .indexOf(juegoId);

        //inicia nueva ronda
        if (juegos[index].ronda < 15) {
            juegos[index].ronda = Number(juegos[index].ronda) + 1;
            juegos[index].lanzamiento = 0;

            //limpiar dado
            for(let i=0; i<5; i++){
                juegos[index].dado[i] = 0;
            }
        }

        return juegos;
    }
};
