"use strict";

// todas las funcionalidades del juego
let juegoNuevo = require("./juegoNuevo.js");
let rondaNuevo = require("./RondaNuevo.js");
let lanzarDados = require("./lanzarDados.js");
let rondaFin = require("./RondaFin.js");
let juegoFin = require("./juegoFin.js");

module.exports = {
    juegoNuevo: (juegos, nombreJugador) => {
        return juegoNuevo.execute(juegos, nombreJugador);
    },

    rondaNuevo: (juegoId, juegos) => {
        return rondaNuevo.execute(juegoId, juegos);
    },

    lanzarDados: (reqbody, juegos, tirarDado, ai, calcular) => {
        return lanzarDados.execute(reqbody, juegos, tirarDado, ai, calcular);
    },

    rondaFin: (juegoId, resultadoSeleccionado, juegos, calcular) => {
        return rondaFin.execute(juegoId, resultadoSeleccionado, juegos, calcular);
    },

    juegoFin: (juegoId, juegos) => {
        return juegoFin.execute(juegoId, juegos);
    }

}