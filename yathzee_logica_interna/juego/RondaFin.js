"use strict";
module.exports = {
    execute: (juegoId, resultadoSeleccionado, juegos, calcular) => {
        let index = juegos
            .map(o => {
                return o.juegoId;
            })
            .indexOf(juegoId);

        //agregar resultadoSeleccionado a la tabla
        let calculatedTable = calcular.calcular(juegos[index].dado);
        const checkList = [
            "unos",
            "doses",
            "treses",
            "cuatros",
            "cincos",
            "seises",
            "tresIguales",
            "cuatroIguales",
            "escaleraCorta",
            "escaleraLarga",
            "full",
            "chance",
            "yatzy"
        ];
        checkList.forEach(check => {
            if (
                resultadoSeleccionado == check &&
                juegos[index].tablaJuego[check + "Logrado"] == false
            ) {
                juegos[index].tablaJuego[check] = calculatedTable[check];
                juegos[index].tablaJuego[check + "Logrado"] = true;
            }
        });

        // Calcular bonus y total de puntos.
        let calc =
            juegos[index].tablaJuego.unos +
            juegos[index].tablaJuego.doses +
            juegos[index].tablaJuego.treses +
            juegos[index].tablaJuego.cuatros +
            juegos[index].tablaJuego.cincos +
            juegos[index].tablaJuego.seises;
        juegos[index].tablaJuego.bonus = calcular.bonus(calc);
        juegos[index].total =
            calc +
            juegos[index].tablaJuego.bonus +
            juegos[index].tablaJuego.tresIguales +
            juegos[index].tablaJuego.cuatroIguales +
            juegos[index].tablaJuego.escaleraCorta +
            juegos[index].tablaJuego.escaleraCorta +
            juegos[index].tablaJuego.full +
            juegos[index].tablaJuego.chance +
            juegos[index].tablaJuego.yatzy;
        return juegos;
    }
};
