"use strict";
module.exports = {
    execute: (reqbody, juegos, tiroDado, ia, calcular) => {

        let juegoId = reqbody.juegoId;
        let first = reqbody.uno;
        let second = reqbody.dos;
        let third = reqbody.tres;
        let fourth = reqbody.cuatro;
        let fifth = reqbody.cinco;
        let index = juegos
            .map(o => {
                return o.juegoId;
            })
            .indexOf(juegoId);
        juegos[index].lanzamiento++;

        // lanzamiento all if first round. Otherwise select which to lanzamiento.
        // Do not lanzamiento if lanzamiento number or round number is off the scale of yatzy rules.
        if (juegos[index].lanzamiento < 4 && juegos[index].ronda < 16) {
            if (juegos[index].lanzamiento == 1 || first == false) {
                juegos[index].dado[0] = tiroDado.lanzarDado();
            }
            if (juegos[index].lanzamiento == 1 || second == false) {
                juegos[index].dado[1] = tiroDado.lanzarDado();
            }
            if (juegos[index].lanzamiento == 1 || third == false) {
                juegos[index].dado[2] = tiroDado.lanzarDado();
            }
            if (juegos[index].lanzamiento == 1 || fourth == false) {
                juegos[index].dado[3] = tiroDado.lanzarDado();
            }
            if (juegos[index].lanzamiento == 1 || fifth == false) {
                juegos[index].dado[4] = tiroDado.lanzarDado();
            }
        } else {
            juegos[index].lanzamiento = 3;
        }

        // INSERTAR IA AQUI.


        return juegos;
    }

}