"use strict"
let juegoId = 0;

module.exports = {

    execute: (juegos, nombreJugador) => {
        juegoId++;

        const tablaJuego = {
            unos: 0,
            doses: 0,
            treses: 0,
            cuatros: 0,
            cincos: 0,
            seises: 0,
            bonus: 0,
            tresIguales: 0,
            cuatroIguales: 0,
            full: 0,
            escaleraCorta: 0,
            escaleraLarga: 0,
            yatzy: 0,
            chance: 0,

            tresIgualesLogrado: false,
            cuatroIgualesLogrado: false,
            fullLogrado: false,
            escaleraCortaLogrado: false,
            escaleraLargaLogrado: false,
            yatzyLogrado: false,
            chanceLogrado: false,
        };
        juegos.push({
            juegoId: juegoId,
            nombreJugador: nombreJugador,
            ronda: 0,
            lanzamiento: 0,
            total: 0,
            dado: [0,0,0,0,0],
            tablaJuego:tablaJuego
        });

        return juegos;


    }
}
