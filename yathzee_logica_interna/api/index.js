"use strict";
let juegos = [];
let puntajesAltos = [];

module.exports = (calculator, tirarDado, juego, puntajeAlto, app, ia) => {
    // agregar nuevo juego
    app.post("/api/juegoNuevo", (req, res) => {
        console.log(Date() + " - POST: /api/juegoNuevo. request:", req.body);
        juegos = juego.juegoNuevo(juegos, req.body.nombreJugador);
        res.status(200).json(juegos[juegos.length - 1]);
    });

    // iniciar nueva ronda
    app.post("/api/nuevaRonda", (req, res) => {
        console.log(Date() + " - POST: /api/nuevaRonda. request:", req.body);
        juegos = juego.rondaNuevo(req.body.juegoId, juegos);
        res.status(200).json(juegos[req.body.juegoId - 1]);
    });

    // lanzar dado
    app.post("/api/lanzarDado", (req, res) => {
        console.log(Date() + " - POST: /api/lanzarDado. request:", req.body);
        juegos = juego.lanzarDados(req.body, juegos, tirarDado, ia, calculator);
        res.status(200).json(juegos[req.body.juegoId - 1]);
    });

    // fin ronda
    app.post("/api/finRonda", (req, res) => {
        console.log(Date() + " - POST: /api/finRonda. request:", req.body);
        juegos = juego.rondaFin(
            req.body.juegoId,
            req.body.resultadoSeleccionado,
            juegos,
            calculator
        );
        res.status(200).json(juegos[req.body.juegoId - 1]);
    });

    // fin juego
    app.post("/api/finJuego", (req, res) => {
        console.log(Date() + " - POST: /api/finJuego. request:", req.body);
        juegos = juego.juegoFin(req.body.juegoId, juegos);
        puntajesAltos = puntajeAlto.setPuntajeAlto(
            puntajes,
            juegos[req.body.juegoId - 1].puntajeAlto
        );
        puntajesAltos = puntajeAlto.ordenarPuntajesAltos(puntajes);
        res.status(200).json(juegos[req.body.juegoId - 1]);
    });

};