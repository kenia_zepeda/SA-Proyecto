# Logica del juego

los endpoints que veo necesarios segun el juego y por la parte grafica:

1. debe existir un metodo get que reenvie la peticion a el microservicio de los dados, el cual es independiente. La forma mas facil es utilizando axios.

2. debe existir un metodo que recibe el array de los dados y en base a estos se calculara las casillas que pueden elegirse. por lo tanto cuando se llame a este endpoint unicamente con la jugada actual inicializara un array de juego y lo enviara a la interfaz. Las casillas se inicializar con -1 y unicamente se llenan las que sean permitidas.

3. un endpoint en el cual se pueda enviar la eleccion del jugador luego de ver las posibilidades.

4. un endpoint en el cual se pueda solicitar el tablero de juego del otro jugador.

5. un endpoint en el cual se pueda retornar el juego del jugador.

6. los puntos 4 y 5 son los metodos crusados para el juego.

7. un endpoint que retorne el total de juegos que tiene el jugador pendiente.