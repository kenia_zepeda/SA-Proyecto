"use strict";
module.exports = {


    calcular: dado => {
        let results = {
            unos: calcular(dado, 1),
            doses: calcular(dado, 2),
            treses: calcular(dado, 3),
            cuatros: calcular(dado, 4),
            cincos: calcular(dado, 5),
            seises: calcular(dado, 6),
            tresIguales: tresDeUnTipo(dado),
            cuatroIguales: cuatroDeUnTipo(dado),
            escaleraCorta: escaleraCorta(dado),
            escaleraLarga: escaleraLarga(dado),
            full: full(dado),
            chance: chance(dado),
            yatzy: yatzi(dado)
        };
        return results;
    },

    // calcular if player is entitled to bonus.
    bonus: (totalSum) => {
        let results = bonus(totalSum);
        return results;
    }

}

var calcular = (dado, numero) => {
    let puntos = 0;
    dado.forEach(result => {
        if (result == numero) {
            puntos += result;
        }
    });
    return puntos;
};

var yatzi = (dado) => {
    let foundResult = 0;
    let found = 0;
    for (let j = 6; j > 0; j--) {
        found = 0;
        for (let i = 0; i < dado.length; i++) {
            if (dado[i] == j) {
                found++;
                foundResult = Math.floor(parseInt(dado[i]) * (5));
            }
        }
        if (found > 4) {
            return 50;
        }
    }
    return 0;
};

var tresDeUnTipo = (dado) => {
    let foundResult = 0;
    let found = 0;
    for (let j = 6; j > 0; j--) {
        found = 0;
        for (let i = 0; i < dado.length; i++) {
            if (dado[i] == j) {
                found++;
                foundResult = Math.floor(parseInt(dado[i]) * (3));
            }
        }
        if (found > 2) {
            return foundResult;
        }
    }
    return 0;
};

var cuatroDeUnTipo = (dado) => {
    let foundResult = 0;
    let found = 0;
    for (let j = 6; j > 0; j--) {
        found = 0;
        for (let i = 0; i < dado.length; i++) {
            if (dado[i] == j) {
                found++;
                foundResult = Math.floor(parseInt(dado[i]) * (4));
            }
        }
        if (found > 3) {
            return foundResult;
        }
    }
    return 0;
};


// verificar que cada item en el array es unico
const checkIfArrayIsUnique = (myArray) => {
    return myArray.length === new Set(myArray).size;
};

// verificar si el array (=a) contiene string (=obj).
const contains = (a, obj) => {
    var i = a.length;
    while (i--) {
        if (a[i] === obj) {
            return true;
        }
    }
    return false;
};

var escaleraLarga = (dado) => {
    if (checkIfArrayIsUnique(dado) == true) {
        if (contains(dado, 1) == false && contains(dado, 6) == true) {
            return 40;
        }
    }
    return 0;
};

var escaleraCorta = (dado) => {
    if (checkIfArrayIsUnique(dado) == true) {
        if (contains(dado, 1) == true && contains(dado, 6) == false) {
            return 30;
        }
    }
    return 0;
};

var full = (dado) => {
    let found = 0;
    let foundResult2 = 0;
    let foundResult3 = 0;
    let foundResultTotal = 0;
    let three = false;
    let two = false;
    for (let j = 6; j > 0; j--) {
        found = 0;
        for (let i = 0; i < dado.length; i++) {
            if (dado[i] == j) {
                found++;
                foundResult2 = Math.floor(parseInt(dado[i]) * 2);
                foundResult3 = Math.floor(parseInt(dado[i]) * 3);
            }
        }
        if (found > 1) {
            if (found == 2 && two == false) {
                foundResultTotal += foundResult2;
                two = true;
            }
            if (found == 3 && three == false) {
                foundResultTotal += foundResult3;
                three = true;
            }
            if (two == true && three == true) {
                return foundResultTotal;
            }
        }
    }
    return 0;
};

var chance = (dado) => {
    let points = 0;
    dado.forEach(result => {
        points += result;
    });
    return points;
};

var bonus = (amount) => {
    let bonus = 0;
    if (amount >= 63) {
        bonus = 50;
    }
    return bonus;
};


