CREATE TABLE juego(
  id_juego INTEGER NOT NULL AUTO_INCREMENT,
  link VARCHAR(500) NOT NULL,

  PRIMARY KEY (id_juego)
);

CREATE TABLE torneo(
  id_torneo INTEGER NOT NULL AUTO_INCREMENT,
  id_juego INTEGER FOREIGN KEY REFERENCES juego(id_juego),
  n_partidas INTEGER NOT NULL,

  PRIMARY KEY (id_torneo)
);

CREATE TABLE partido(
  id_partido VARCHAR(100) NOT NULL,
  id_torneo INTEGER FOREIGN KEY REFERENCES torneo(id_torneo),

  PRIMARY KEY (id_partido)
);

CREATE TABLE jugador(
  id_jugador INTEGER NOT NULL,
  nombre VARCHAR(100) NOT NULL,

  PRIMARY KEY (id_jugador)
);

CREATE TABLE llave(
  id_partido INTEGER FOREIGN KEY REFERENCES partido(id_partido),
  id_jugador1 INTEGER FOREIGN KEY REFERENCES jugador(id_jugador),
  id_jugador2 INTEGER FOREIGN KEY REFERENCES jugador(id_jugador),
  score_jugador1 INTEGER NOT NULL DEFAULT 0,
  score_jugador2 INTEGER NOT NULL DEFAULT 0,

  PRIMARY KEY (id_partido, id_jugador1, id_jugador2)
);