# SA-Proyecto
Proyecto de Laboratorio


## Comandos importantes

```
>>> sudo docker logs -f container_name
>>> sudo docker exec -it container_name bash
>>> sudo docker exec -it container_name sh
>>> sudo docker build -t name:tag .
>>> sudo docker-compose up -d
>>> sudo docker-compose down

>>> mysql -h localhost -P 3306 --protocol=tcp -u root -p
>>> mysql -u root -p

>>> sudo docker container prune
>>> sudo docker volume prune
>>> sudo docker network prune
```