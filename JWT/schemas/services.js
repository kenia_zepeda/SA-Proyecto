const mongoose = require('mongoose');

const servicio = new mongoose.Schema({
  id: {type:String,trim:true},
  secret:{type:String}
});


module.exports = mongoose.model('microservicios',servicio);