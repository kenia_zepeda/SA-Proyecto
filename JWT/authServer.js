require("dotenv").config();
'use strict';
var mongoose = require('mongoose');
const fs   = require('fs');
const express = require("express");
const app = express();
const jwt = require("jsonwebtoken");
const bodyParser = require('body-parser');

//conecction a la base de datos de mongo
mongoose.connect(process.env.DB_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true
}).
catch(error => handleError(error));
// PRIVATE and PUBLIC key
var privateKEY  = fs.readFileSync('./private.key', 'utf8');
//lleve publica para poder descriptar los jwt enviador al servidor
var publicKEY  = fs.readFileSync('./public.key', 'utf8');

//importar los schemas
const microservicios = require('./schemas/services'); // importa el esquema de microservicios
const scopes = require('./schemas/scopes'); // importa el esquema de scopes de cada microservicios

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.json());
var exp = Math.floor(new Date().getTime() / 1000) + (60 * 60 * 24 * 2);
let payload = {
    alg: 'RS256',
    typ: "JWT",
    exp: exp,
    scope: []
}
var signOptions = {
  algorithm:  "RS256"   // RSASSA [ "RS256", "RS384", "RS512" ]
 };


app.post("/token", (req, res) => {
  //obtenemos los valores del request id_cliente, secret
  const id_servicio = req.query.id;
  const secret = req.query.secret;
  /* Aqui miramos si la info en el query
      es valida y sino mandamos estatus 400

  */
  if(id_servicio == null || secret==null) return res.sendStatus(400)
  
  //encontramos el servicio usando la id que nos manda el cliente y validar si esta
  microservicios.findOne({id:id_servicio},{_id:0,id:1,secret:1}, function(err, result) {
    //si hay error en la obtencion del dato mandamos estatus 400
     if (err) {
      res.sendStatus(400)
          } else {
            if(result!=null){// sino hay error pero no exist el dato mandmso 400 de token no valido
             if(result.id == id_servicio && result.secret == secret){// si hay resultado entonces comparamos con la bd el token
       
                //obtenemos el scope segun el servicio y lo metemos en el payload y lo encriptamos
                scopes.findOne({id:id_servicio},{_id:0,scope:1}, function(err, result) {
                          if (err){
                                  res.sendStatus(400)
                                  }else{
                                      var js=JSON.stringify(result);
                                      var obj = JSON.parse(js);        
                                      payload.scope=obj.scope; 
                                      //creamos el token con la info de scope y de id            
                                      let accessToken = jwt.sign(payload,privateKEY,signOptions);
                                      res.status(201).json({jwt:accessToken});               
                                       }
                });
     
              }else{
                  res.sendStatus(400);//error por que son iguales en la bd
                    }
            }else{
                  res.sendStatus(400);// error por que no encontro el token
              }
      }
  });

});
 
 
 

app.get("/", (req, res) => {
  res.send("SERVIDOR JWT ENCENDIDO");
});
var verifyOptions = {
  algorithm:  "RS256"
 };

 //ruta para usar el desencriptar el token dado por el servidor
 // se usa el verifyOptions para ver si el algoritmo es rs256 usando la llave publica
app.post('/decode', (req, res)=>{
  var legit = jwt.verify(req.body.token, publicKEY, verifyOptions);
  //vemos el valor desencriptado en json y ver su data
  res.json(legit)

});

app.listen(process.env.PORTJWT);
