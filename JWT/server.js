require('dotenv').config()
const express = require("express");
const app = express();

const jwt = require("jsonwebtoken");
app.use(express.json());

const posts = [
  {
    username: "Gary",
    title: "post 1",
  },
  {
    username: "Danny",
    title: "post 2",
  },
];

app.get("/posts",authenticationToken,(req, res) => {
  res.json(posts.filter(post => post.username === req.user.name));
});
function authenticationToken(req, res, next){

    const autheader =  req.headers['authorization']
    const token = autheader && autheader.split(' ')[1]
    if( token == null) return res.sendStatus(400) 

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET,(err, user)=>{
        if (err) return res.sendStatus(403)
        req.user = user
        next()


    })
}

app.listen(3000);
