"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var fs = require('fs');
var axios = require('axios');
const mysql = require('mysql');
const app = express();
app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
const con = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    //ALTER USER 'admdb'@'localhost' IDENTIFIED WITH mysql_native_password BY '123456789'
    //user: 'admdb',
    //password: '1234567',
    user: 'root',
    password: 'root',
    database: 'torneo'
});
con.connect((err) => {
    if (err) {
        console.log('Error connecting to database' + err);
        return;
    }
    console.log('Connection to database established');
});
/*Variables de entorno donde correra la API Torneo*/
const ip = process.env.IP || "localhost";
const port = process.env.PORT || 9000;
const ipUser = "localhost";
const portUser = 3000;
// define a route handler for the default home page
app.get("/", (req, res) => {
    res.send("Hello world!");
});
app.post("/CrearTorneo/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var idJuego = req.body.Jid;
    var url = req.body.Url;
    var jugadores = req.body.Jugadores;
    var cantJug = jugadores.length;
    if (cantJug % 2 == 0) {
        var partidas = cantJug / 2;
        var partidasAreas = partidas / 2; //2 areas
        var json = { 406: "No se comunicó con la base de datos" };
        //crearTorneo
        var idTorneo = yield crearTorneo(idJuego, partidas);
        var partidasIds = new Array();
        //random array jugadores
        var cont1 = -2;
        var cont2 = -1;
        jugadores.sort(function (a, b) { return (Math.random() - 0.5); });
        for (let index = 0; index < partidas; index++) {
            cont1 = cont1 + 2;
            cont2 = cont2 + 2;
            const seleccion1 = jugadores[cont1];
            const seleccion2 = jugadores[cont2];
            var idPartido = yield crearPartidas(idTorneo);
            partidasIds.push(Number(idPartido));
            var llave = yield crearLlaves(seleccion1, seleccion2, idPartido);
        }
        //crear el resto de partidas
        //cuartos de final
        //semifinal
        partidas = partidas / 2;
        while (partidas >= 1) {
            //Crear partida
            for (let index = 0; index < partidas; index++) {
                var idPartido = yield crearPartidas(idTorneo);
                partidasIds.push(Number(idPartido));
                //Crear Llave
                var llave = yield crearLlaves(0, 0, idPartido);
            }
            partidas = partidas / 2;
        }
        json = { 201: "Torneo Creado", "TorneoId": idTorneo, "Partidas:": partidasIds };
        res.send(JSON.stringify(json));
    }
    else {
        json = { 406: "Número de jugadores inválido" };
        res.send(JSON.stringify(json));
    }
}));
function crearTorneo(idJuego, partidas) {
    return new Promise((resolve, reject) => {
        const query = 'INSERT INTO `torneo`(`id_juego`,`n_partidas`) VALUES (' + idJuego + ',' + partidas + ');';
        con.query(query, (err, res) => {
            if (err)
                return;
            // json = {201:"Torneo Creado","torneoId":res.insertId};
            console.log('Insert Torneo, Last insert ID:', res.insertId);
            resolve(res.insertId);
        });
    });
}
function crearPartidas(idTorneo) {
    return new Promise((resolve, reject) => {
        const query1 = 'INSERT INTO `partido`(`id_torneo`) ' +
            'VALUES(' + idTorneo + ');';
        con.query(query1, (err, res) => {
            if (err)
                return;
            console.log('Insert Partido, Last insert ID:', res.insertId);
            resolve(res.insertId);
        });
    });
}
function crearLlaves(seleccion1, seleccion2, idPartido) {
    return new Promise((resolve, reject) => {
        const query2 = 'INSERT INTO `llave`(`id_partido`,`id_jugador1`,`id_jugador2`,`score_jugador1`,`score_jugador2`)\n' +
            'VALUES(' + idPartido + ',' + seleccion1 + ',' + seleccion2 + ',0,0);';
        con.query(query2, (err, res) => {
            if (err)
                return;
            console.log('Insert Llave, jugadores: ' + seleccion1 + ',' + seleccion2);
            resolve(0);
        });
    });
}
app.get("/ListaJuego/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var json = "";
    con.query('SELECT * FROM juego', (err, rows) => {
        if (err) {
            json = { 406: "Error en realizar consulta de juegos" };
            res.send(JSON.stringify(json));
            console.log('Error en realizar consulta de juegos' + err);
            return;
        }
        console.log('Juego Data received from Db:');
        console.log(rows);
        json = { 201: "Listado de juegos", "Juegos": rows };
        res.send(JSON.stringify(json));
    });
    /***
     * Ejemplo de response
     * {"201":"Listado de juegos",
     * "Juegos":[{"id_juego":1,"link":"juego1","nombre":"juego1"},{"id_juego":2,"link":"juego2","nombre":"juego2"},{"id_juego":3,"link":"juego3","nombre":"juego3"}]}
     ***/
}));
app.post("/InsertJuego/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var json = "";
    const query = 'INSERT INTO `juego`(`link`,`nombre`)' +
        'VALUES(\'' + req.body.Link + '\',\'' + req.body.Nombre + '\');';
    con.query(query, (err, res1) => {
        if (err) {
            json = { 406: "Error en realizar insert de juegos" };
            res.send(JSON.stringify(json));
            console.log('Error en realizar insert de juegos' + err);
            return;
        }
        json = { 201: "Juego insertado", "Jid": res1.insertId };
        console.log('Insert Juego, Last insert ID:', res1.insertId);
        res.send(JSON.stringify(json));
    });
    /**
     * Ejemplo del body del request
     * {"Nombre":"Juego10","Link":"http://linkjuego"}
     */
}));
app.post("/UpdateJuego/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var json = "";
    const query = 'UPDATE `juego`\n' +
        'SET\n' +
        '`link` = \'' + req.body.Link + '\',\n' +
        '`nombre` = \'' + req.body.Nombre + '\'\n' +
        'WHERE `id_juego` =' + req.body.Jid + ';';
    con.query(query, (err, res1) => {
        if (err) {
            json = { 406: "Error en realizar update de juegos" };
            res.send(JSON.stringify(json));
            console.log('Error en realizar update de juegos' + err);
            return;
        }
        json = { 201: "Juego actualizado", "Jid": res1.changedRows };
        console.log('Insert Juego, Last update rows:', res1.changedRows);
        res.send(JSON.stringify(json));
    });
    /**
     * Ejemplo del body del request
     * {"Jid":"1","Nombre":"Juego10","Link":"http://linkjuego"}
     */
}));
app.post("/DeleteJuego/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var json = "";
    const query = 'DELETE FROM `juego`\n' +
        'WHERE `id_juego` =' + req.body.Jid + ';';
    con.query(query, (err, res1) => {
        if (err) {
            json = { 406: "Error en realizar delete de juegos" };
            res.send(JSON.stringify(json));
            console.log('Error en realizar delete de juegos' + err);
            return;
        }
        json = { 201: "Juego eliminado", "Jid": res1.affectedRows };
        console.log('Insert Juego, Last delete rows:', res1.affectedRows);
        res.send(JSON.stringify(json));
    });
    /**
     * Ejemplo del body del request
     * {"Jid":"1"}
     */
}));
app.get("/ListaUsuario/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var config = {
        method: 'get',
        url: ipUser + portUser + '/jugadores',
        headers: {
            'Content-Type': 'application/json'
        }
    };
    axios(config)
        .then(function (res1) {
        console.log(JSON.stringify(res1.data));
        res.send(res1.data);
    })
        .catch(function (error1) {
        console.log(error1);
    });
}));
app.get("/InsertUsuario/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var data = JSON.stringify("{\"email\":\"" + req.body.Email + "\"," +
        "\"password\":\"" + req.body.Password + "\"," +
        "\"nombres\":\"" + req.body.Nombres + "\"," +
        "\"apellidos\":\"" + req.body.Apellidos + "\"," +
        "\"administrador\":\"" + req.body.Administrador + "\"" +
        "}");
    var config = {
        method: 'post',
        url: ipUser + portUser + '/jugadores',
        headers: {
            'Content-Type': 'application/json'
        },
        data: data
    };
    axios(config)
        .then(function (res1) {
        console.log(JSON.stringify(res1.data));
        res.send(res1.data);
    })
        .catch(function (error1) {
        console.log(error1);
    });
}));
app.get("/UpdateUsuario/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var data = JSON.stringify("{\"email\":\"" + req.body.Email + "\"," +
        "\"password\":\"" + req.body.Password + "\"," +
        "\"nombres\":\"" + req.body.Nombres + "\"," +
        "\"apellidos\":\"" + req.body.Apellidos + "\"," +
        "\"administrador\":\"" + req.body.Administrador + "\"" +
        "}");
    var config = {
        method: 'put',
        url: ipUser + portUser + '/jugadores?id=' + req.body.Id,
        headers: {
            'Content-Type': 'application/json'
        },
        data: data
    };
    axios(config)
        .then(function (res1) {
        console.log(JSON.stringify(res1.data));
        res.send(res1.data);
    })
        .catch(function (error1) {
        console.log(error1);
    });
}));
app.get("/DeleteUsuario/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var json = "";
}));
app.get("/Login/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var data = JSON.stringify("{\"email\":\"" + req.body.Email + "\"," +
        "\"password\":\"" + req.body.Password + "\"}");
    var config = {
        method: 'get',
        url: ipUser + portUser + '/login',
        headers: {
            'Content-Type': 'application/json'
        },
        data: data
    };
    axios(config)
        .then(function (res1) {
        console.log(JSON.stringify(res1.data));
        res.send(res1.data);
    })
        .catch(function (error1) {
        console.log(error1);
    });
}));
app.listen(port, ip, () => __awaiter(void 0, void 0, void 0, function* () {
    console.log('Torneo se escucha en el puerto: %d y con la ip: %s', port, ip);
}));
